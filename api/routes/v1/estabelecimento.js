const estabelecimentoController = require('../../controllers/estabelecimento.controller')
const {requirePermissionsMiddleware,verifyJWT,verifyJWTOptional,databaseFinder} = require('../../utils/middlewares/permission.middleware')
const {estabelecimentoMiddleware,estabelecimentoPermFileMiddleware} = require('../../utils/middlewares/estabelecimento/estabelecimento.middleware')

const fileUploadMiddlare = require('../../utils/middlewares/file-upload.middleware')
module.exports = (router) => {

    router.route('/estabelecimento')
    .get(verifyJWTOptional,databaseFinder('estabelecimento','VIEW_ALL_ESTABLISHMENT'),
    estabelecimentoController.lista
    )
    .post(verifyJWT,requirePermissionsMiddleware(["CREATE_ESTABLISHMENT"]),estabelecimentoMiddleware,fileUploadMiddlare('estabelecimento'),
    estabelecimentoController.createEstabelecimento
    )
    router.route('/estabelecimento/:id')
    .put(verifyJWT,requirePermissionsMiddleware(["EDIT_ESTABLISHMENT"]),estabelecimentoMiddleware,databaseFinder('estabelecimento','VIEW_ALL_ESTABLISHMENT'),fileUploadMiddlare('estabelecimento'),
    estabelecimentoController.editEstabelecimento
    )
    router.route('/estabelecimento/:id')
    .delete(verifyJWT,requirePermissionsMiddleware(["DELETE_ESTABLISHMENT"]),databaseFinder('estabelecimento','VIEW_ALL_ESTABLISHMENT'),
    estabelecimentoController.delEstabelecimento
    )
    router.route('/estabelecimento/search')
    .get(
        estabelecimentoController.search
        )
        router.route('/estabelecimento/:id')
        .get(verifyJWTOptional,
            estabelecimentoController.lista
            )
        }