
const usuarioController = require('../../controllers/usuario.controller');
const validaDTO = require('../../utils/middlewares/validate-dto.middleware');
const {requirePermissionsMiddleware,verifyJWT,verifyJWTUpdateUser,verifyJWTOptional} = require('../../utils/middlewares/permission.middleware')
const { usuarioMiddleware } = require('../../utils/middlewares/usuario/usuario.middleware')
const fileUploadMiddlare = require('../../utils/middlewares/file-upload.middleware')

const joi = require('joi');

module.exports = (router) => {

  router.route('/user/auth')
  .post(
    validaDTO('body', {
      email: joi.string().required().messages({
        'any.required': `"e-mail" é um campo obrigatório`,
        'string.empty': `"e-mail" não deve ser vazio`,
      }),
      senha: joi.string().required().messages({
        'any.required': `"senha" é um campo obrigatório`,
        'string.empty': `"senha" não deve ser vazio`,
      })
    }),
    usuarioController.auth
    )
    router.route('/user/register')
    .post(
      validaDTO('body', {
        email: joi.string().required().messages({
          'any.required': `"e-mail" é um campo obrigatório`,
          'string.empty': `"e-mail" não deve ser vazio`,
        }),
        senha: joi.string().required().messages({
          'any.required': `"senha" é um campo obrigatório`,
          'string.empty': `"senha" não deve ser vazio`,
        }),
        cargo: joi.string().messages({
          'any.required': `"cargo" é um campo obrigatório`,
          'string.empty': `"cargo" não deve ser vazio`,
        }),
        cpf: joi.string().required().messages({
          'any.required': `"cpf" é um campo obrigatório`,
          'string.empty': `"cpf" não deve ser vazio`,
        }),
        nome: joi.string().required().messages({
          'any.required': `"nome" é um campo obrigatório`,
          'string.empty': `"nome" não deve ser vazio`,
        }),
        date: joi.date().required().messages({
          'any.required': `"date" é um campo obrigatório`,
          'string.empty': `"date" não deve ser vazio`,
        }),
      }),
      usuarioController.createUser
      )
      router.route('/user/email/reset')
      .post(verifyJWTUpdateUser('x-email','email'),
        usuarioController.resetEmail
        )
      router.route('/user/password/reset')
      .post(verifyJWTUpdateUser('x-password','senha'),
        usuarioController.resetPassword
        )
        router.route('/user/email/recovery')
        .post(
          usuarioController.recoveryEmail
          )
        router.route('/user/password/recovery')
        .post(
          usuarioController.recoveryPassword
          )
          router.route('/user')
          .get(
            usuarioController.lista
            )
            router.route('/user/:id')
            .get(verifyJWTOptional,
              usuarioController.lista
              )
              router.route('/user/:id')
              .delete(
                usuarioController.delUser
                )
                router.route('/user/:id')
                .put(verifyJWT,usuarioMiddleware,
                validaDTO('body', {
                  email: joi.string().messages({
                    'any.required': `"e-mail" é um campo obrigatório`,
                    'string.empty': `"e-mail" não deve ser vazio`,
                  }),
                  senha: joi.string().messages({
                    'any.required': `"senha" é um campo obrigatório`,
                    'string.empty': `"senha" não deve ser vazio`,
                  }),
                  cargo: joi.string().messages({
                    'any.required': `"senha" é um campo obrigatório`,
                    'string.empty': `"senha" não deve ser vazio`,
                  }),
                  cpf: joi.string().messages({
                    'any.required': `"senha" é um campo obrigatório`,
                    'string.empty': `"senha" não deve ser vazio`,
                  }),
                  nome: joi.string().messages({
                    'any.required': `"senha" é um campo obrigatório`,
                    'string.empty': `"senha" não deve ser vazio`,
                  }),
                  date: joi.date().messages({
                    'any.required': `"senha" é um campo obrigatório`,
                    'string.empty': `"senha" não deve ser vazio`,
                  }),
                }),fileUploadMiddlare('usuario',false),
                usuarioController.editUser
                )

              }