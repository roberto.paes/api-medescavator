const path = require('path');
const root_dir  = path.join(path.dirname(require.main.filename),'arquivos')

const fs = require('fs');
const uuid = require('uuid').v4;

const criaEndereco = (destino, arquivoNome) => {
    return path.join(root_dir, destino, arquivoNome);
}

const criaNome = (tipo) => {
    const tipoTratado = tipo.split('/')[1];
    return `${uuid()}.${tipoTratado}`;
}

const move = async (temporario, definitivo) => {
    return fs.renameSync(temporario, definitivo);
}
const substituir = async (temporario,dir,arquivo) => {
    console.log('#######overwrite',temporario,dir,path.join(root_dir,dir,arquivo))
    return fs.renameSync(temporario, path.join(root_dir,dir,arquivo));
}
const remove = async (arquivo,dir) => {
    return fs.unlinkSync(path.join(root_dir,dir,arquivo));
}
const fileExist = async (arquivo,dir) => {
    let aqr = path.join(root_dir,dir,arquivo)
    if (fs.existsSync(aqr)) {
        return true
    }else{
        return false
    }

}


module.exports = {
    criaEndereco,
    substituir,
    fileExist,
    criaNome,
    move,
    remove
}
