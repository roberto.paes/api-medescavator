const { Schema } = require("mongoose");

module.exports = {
  titulo: {
    type: String,
    required: true,
  },
  comentario: {
    type: String,
    required: false,
  },
  tipo:{
    type: Number,
    required: true,
  },
  relevante:{
    type: Number,
    default: 0,
    required: false,
  },
  status: {
    type: Boolean,
    default: false,
    required: false,
  },
  usuarioVotante: [{ type : Schema.ObjectId, ref: 'usuario', required:false }],

  usuario: { type : Schema.ObjectId, ref: 'usuario', required:true },
  produto: { type : Schema.ObjectId, ref: 'produto', required:true }


}
