const { usuario  } = require('../models/index');
const getUserInfo= async (_id) => {
  return await usuario.findOne({ _id:id })
}
const validEmail = async (email) => {
  return await usuario.findOne({ email }) ? true : false
}
const validCpf = async (cpf) => {
  return await usuario.findOne({ cpf }) ? true : false
}
const validCNPJ = async (cnpj) => {
  return await usuario.findOne({ cnpj }) ? true : false
}

module.exports = {
  validEmail,
  validCpf,
  validCNPJ,
  getUserInfo
}