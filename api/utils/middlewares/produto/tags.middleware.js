const {tagsGenerator} = require('../../search-engine')
const { estabelecimento,categoria,produto } = require('../../../models/index');


const tagsUpdateMiddleware = async (route,req) => {


    const _id = req.params.id
    let allProduct
    switch (route){
        case 'produto':
        allProduct = await produto.find({_id}).populate("estabelecimento").populate('categorias')
        break
        case 'categoria':
        allProduct = await produto.find({categorias:_id}).populate("estabelecimento").populate('categorias')
        break
        case 'estabelecimento':
        allProduct = await produto.find({estabelecimento:_id}).populate("estabelecimento").populate('categorias')
        break
    }

    await  Promise.all(allProduct.map(async (product)=>{
        let relationTags = []
        if(Array.isArray(product['categorias'])){
            Promise.all(product['categorias'].map((item)=>{
                tagsGenerator(item,['nome']).map((v)=>{
                    relationTags.push(v)
                })
            }))
        }else{
            tagsGenerator(product['categorias'],['nome']).map((v)=>{
                relationTags.push(v)
            })
        }
        tagsGenerator(product,['nome','modelo','fabricante']).map((v)=>{
            relationTags.push(v)
        })
        tagsGenerator(product['estabelecimento'],['nome','cnpj']).map((v)=>{
            relationTags.push(v)
        })
        tagsGenerator(product['estabelecimento'].endereco,['estado','estadoSigla','municipio','bairro']).map((v)=>{
            relationTags.push(v)
        })
        await produto.updateOne({_id:product._id}, {
            tags:[...new Set(relationTags)]
        });
    }))
    // console.log('all product',allProduct)





}

const tagsGeneratorMiddleware = (route,tags) => {
    return async function(req, res, next) {

        const body = req.body
        console.log(body)
        var relationTags = tagsGenerator(body,tags)
        switch (route){
            case 'produto':

            if(Array.isArray(body['categorias'])){
                await Promise.all(body['categorias'].map(async (item)=>{
                    await categoria.findOne({_id:item,global:false}, function (err, docs) {
                        if(docs != null){
                            tagsGenerator(docs,['nome']).map((v)=>{
                                relationTags.push(v)
                            })
                        }

                    })
                }))
            }else{
                await categoria.findOne({_id:body['categorias']}, function (err, docs) {
                    if(docs != null){
                        tagsGenerator(docs,['nome']).map((v)=>{
                            relationTags.push(v)
                        })
                    }

                })
            }
            if(body['estabelecimento']){
                await estabelecimento.findOne({_id:body['estabelecimento']}, function (err, docs) {
                    console.log('est',err)
                    if(docs != null){
                        tagsGenerator(docs,['nome','cnpj']).map((v)=>{
                            relationTags.push(v)
                        })
                        tagsGenerator(docs.endereco,['estado','estadoSigla','municipio','bairro']).map((v)=>{
                            relationTags.push(v)
                        })
                    }

                })
            }

            break

        }
        let tagsSingular = [...new Set(relationTags)];
        req.body = {...body,tags:tagsSingular}
        return next()
    }
}
module.exports = {
    tagsUpdateMiddleware,
    tagsGeneratorMiddleware
}