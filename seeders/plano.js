const { Seeder } = require('mongoose-data-seed')
const { plano } = require('../api/models/index')

var data = [
    {
        nome:"Plano gratuito",
        preco:0.00,
        ciclo:0,
        permissions:[],
        extras:['3 imagens por produto','1 loja'],
        descricao:"3 imagens por produto. \n Sem destaque na loja"
    },
    {
        nome:"Plano básico",
        preco:25.00,
        ciclo:'mes',
        extras:['5 imagens por produto','Prioridade 1 nas buscas','3 lojas'],
        permissions:[
            "PROD_IMG_5",
            "RANK_1",
            "EST_3",
            "PREMIUM_SYMBOL"
        ],
        descricao:"Com esse plano você poderá enviar 5 imagens por produto."
    },
    {
        nome:"Plano médio",
        preco:35.00,
        ciclo:'mes',
        extras:['5 imagens por produto','Prioridade 2 nas buscas','5 lojas'],
        permissions:[
            "PROD_IMG_5",
            "RANK_2",
            "EST_5",
            "PREMIUM_SYMBOL"
        ],
        descricao:"Com esse plano você poderá enviar 5 imagens por produto."
    },
    {
        nome:"Plano avançado",
        preco:50.00,
        ciclo:'mes',
        extras:['5 imagens por produto','Prioridade 3 nas buscas','9 lojas'],
        permissions:[
            "PROD_IMG_5",
            "RANK_3",
            "EST_9",
            "PREMIUM_SYMBOL"
        ],
        descricao:"Com esse plano você poderá enviar 5 imagens por produto."
    }
]

class planoSeeder extends Seeder {
    async shouldRun() {
        return plano.countDocuments()
        .exec()
        .then(count => count === 0);
    }

    async run() {
        return plano.create(data);
    }
}

module.exports = {
    planoSeeder
}
