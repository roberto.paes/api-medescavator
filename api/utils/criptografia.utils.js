const md5 = require('md5');
const jwt = require('jsonwebtoken');
const md5HashSecret = process.env.MD5_SECRET;
const jwtHashSecret = process.env.JWT_SECRET;
const jwtTimeLimit = process.env.JWT_VALID_TIME;
const bcrypt = require("bcrypt");
const { usuario  } = require('../models/index');
const criaHash = (senha) => {
  return bcrypt.hash(senha, 4)
}
const validaHash = (senha,hash) => {
  return bcrypt.compare(senha, hash)
}
const validaPasswordChangeHash = async (id) => {
  try{
    let account = await usuario.findById(id)
    return account.senha + "-" + new Date(account.createdAt).getTime()
  }catch(Exception){
    return false
  }
}
const validaEmailChangeHash = async (id) => {
  try{
    let account = await usuario.findById(id)
    return account.email + "-" + new Date(account.createdAt).getTime()
  }catch(Exception){
    return false
  }
}


const criaToken = (model,cargoModel,extraPermission) => {
  let { permission } = cargoModel;
  permission = permission.concat(extraPermission)
  return jwt.sign({ ...model,permission }, jwtHashSecret, {
    expiresIn: `${jwtTimeLimit}ms`,
  })
}
const criaTokenCustom = (model,tempo,hashsecret) => {
  return jwt.sign({ ...model }, hashsecret, {
    expiresIn: `${tempo}ms`,
  })
}

const validaToken = (token) => {
  try {
    return jwt.verify(token, jwtHashSecret);
  } catch (error) {
    return undefined;
  }
}


module.exports = {
  criaHash,
  criaTokenCustom,
  validaPasswordChangeHash,
  validaEmailChangeHash,
  validaHash,
  criaToken,
  validaToken,
}

