const { usuario,cargo, estabelecimento, plano  } = require('../models/index');
const criptografia = require('../utils/criptografia.utils');
const usuarioMapper = require('../mappers/usuario.mapper');
const md5 = require('md5')

const { deleteFile,updateFile } = require('../utils/s3')
const { sendEmailRecovery, sendEmailStatus } = require('../utils/nodemailer')
const { validEmail, validCpf } = require('../utils/user.utils')
//NOVO
const { requirePermission } = require('../utils/middlewares/permission.middleware')

const usuarioEValido = async (email, senha) => {
  let accountExist =   await usuario.findOne({ email }) ? true : false
  if(!accountExist){
    return {status:false, mensagem:'E-mail ou senha errados.'}
  }
  accountExist =  await usuario.findOne({ email })
  if(accountExist.status == 2){
    return {status:false, mensagem:'Sua conta ainda está em análise.'}
  }
  let accountPassword = await criptografia.validaHash(senha,accountExist.senha)
  if(!accountPassword){
    return {status:false, mensagem:'E-mail ou senha errados.'}
  }
  return  {status: await criptografia.validaHash(senha,accountExist.senha)}

}

const criaCredencial = async (usuarioEmail) => {

  const usuarioDB = await usuario.findOne({
    email: usuarioEmail
  });
  const cargoDB = await cargo.findById({ _id: usuarioDB.cargo });
  const getCargo = usuarioMapper.toCargoDTO(cargoDB,usuarioDB.extraPermission)
  const usuarioDTO = usuarioMapper.toUserDTO(usuarioDB);

  return {
    token: criptografia.criaToken(usuarioDTO,cargoDB,usuarioDB.extraPermission),
    cargo:getCargo,
    usuarioDTO,
  };

}

const autenticar = async (email, senha) => {

  const resultadoDB = await usuarioEValido(email, senha);
  if (!resultadoDB.status) {
    return {
      sucesso: false,
      mensagem: resultadoDB.mensagem,
      detalhes: [
        resultadoDB.mensagem,
      ],
    }
  }


  return {
    sucesso: true,
    mensagem: "usuário autenticado com sucesso",
    data: await criaCredencial(email)
  }

}

const cria = async (body) => {
  if(!body.cargo){
    body.cargo = "000000000000000000000000"
  }
  if(body.plano){
    let isPlano = plano.findById(body.plano) ? true : false
    if(isPlano){
      body.cargo = "111111111111111111111111"
    }
  }
  let checkEmail = await validEmail(body.email)
  let checkCpf = await validCpf(body.cpf)

  if(checkEmail || checkCpf){
    return {status:false, mensagem:"Detectamos que você já possui um cadastro."}
  }
  await usuario.create({
    email: body.email,
    cargo: body.cargo,
    nome:body.nome,
    date:body.date,
    extraPermission:body.extraPermission,
    plano:body.plano,
    cpf:body.cpf,
    senha: await criptografia.criaHash(body.senha)
  });
  return {status:true, mensagem:"Cadastro realizado com sucesso. Mas ainda é necessário confirmar seu cadastro."}
}
const edit = async (req) => {
  var update = {...req.body}
  const { id } = req.params
  try{

    if(update['plano']){
      const estabelecimentos = await estabelecimento.find({usuario:id})
      await Promise.all(estabelecimentos.map(async(item)=>{
        await estabelecimento.updateOne({_id:item._id}, {
          rank: req.rank
        });
      }))
    }
    var doc = await usuario.findOneAndUpdate({_id:id}, update)
    if(!doc){
      return false
    }
    if(requirePermission(["DISABLE_USER"],req)){
      let planoName = await plano.findById(doc.plano)
      let cargoName = await cargo.findById(update['cargo'] || doc.cargo)
      let estabelecimentos = await estabelecimento.find({usuario:id})

      let text

      if(update['status'] == 0){
        console.log('status to zero',update['status'])
        doc['status'] = 2
        if(planoName){
          text = `Olá, ${doc.nome}. Obrigado por fazer parte do ${process.env.PROJECT_NAME} e ser nosso ${cargoName.name}.
          Sua conta ${doc.nome} foi desativada, seu plano " ${planoName.nome} " foi congelado e todas as suas lojas estão invisíveis.
          Agradecemos a compreensão. 💔
          `
        }else{
          text = `Olá, ${doc.nome}. Obrigado por fazer parte do ${process.env.PROJECT_NAME}.
          Sua conta ${doc.nome} foi desativada e todas as suas lojas estão invisíveis.
          Agradecemos a compreensão. <3
          `
        }
        await Promise.all(estabelecimentos.map(async (item) =>{
          await estabelecimento.updateOne({_id:item._id},{status:2})
        }))
        sendEmailStatus("Status de conta","Isso é um adeus? " + process.env.PROJECT_NAME,text,doc.email)
      }else if(update['status'] == 1){
        if(planoName){
          text = `Olá, ${doc.nome}. Seja-bem vindo ao ${process.env.PROJECT_NAME} e ser nosso ${cargoName.name}. Somos uma plataforma suuuper planejada para o desenvolvimento da sua loja.
          seu plano " ${planoName.nome} " foi ativo. Agradecemos a preferência. <3
          `
        }else{
          text = `Olá, ${doc.nome}. Seja-bem vindo ao ${process.env.PROJECT_NAME} e ser nosso ${cargoName.name}. Somos Uma plataforma suuuper planejada para o desenvolvimento da sua loja.
          Agradecemos a preferência. <3
          `
        }
        await Promise.all(estabelecimentos.map(async (item) =>{
          await estabelecimento.updateOne({_id:item._id},{status:1})
        }))
        sendEmailStatus("Status de conta","Boas vindas ao " + process.env.PROJECT_NAME,text,doc.email)
      }
    }
    if(update.imagens && update.imagens.length > 0){

      if(doc.imagem){
        let atualImage = []
        atualImage.push(doc.imagem)
        if(atualImage.length > 0){
          updateFile(atualImage, null,'usuario')
        }
      }
      let imagesTest = []
      update.imagens.map((image)=>{
        if(image.mimetype){
          imagesTest.push({nomeOriginal:image.originalname,name:image.filename,tipo:image.mimetype,alt:image.alt || ''})
        }else{
          imagesTest.push({nomeOriginal:image.originalname,name:image.filename,alt:image.alt || ''})
        }
      })
      doc.imagem = imagesTest[0]
    }

    await doc.save()
    // await usuario.findByIdAndUpdate(_id,update,{useFindAndModify:false})
    return true
  }catch(Exception){
    console.log(Exception)
    return false
  }
}

const lista = async (req)=>{
  const { id } = req.params
  console.log('list', req.body)
  try{
    if(id && id == req.userId){
      return await usuario.findById(id).select("-senha").populate('cargo')

    }else if(id && id != req.userId){
      return await usuario.findOne({_id:id,cargo:{$ne:"222222222222222222222222"}}).select("-senha").populate('cargo')
    }else{
      return await usuario.find({cargo:{$ne:"222222222222222222222222"}}).select("-senha").populate('cargo')

    }
  }catch(Exception){
    return false
  }
}

const recoveryPassword = async (req) => {
  const { body } = req
  console.log(body)
  let emailExists = await usuario.findOne({email:body.email}) ? true : false
  if(!emailExists){
    return {status:false, mensagem:"Email não cadastrado."}
  }
  try{
    let account = await usuario.findOne({email:body.email})
    let jwthash = account.senha + "-" + new Date(account.createdAt).getTime()
    let token =  criptografia.criaTokenCustom({id:account._id},600000,jwthash)
    let url = `${process.env.FRONT_URL}/reset/password/${account._id}/${token}`
    console.log(url)
    await sendEmailRecovery("Recuperação de senha","Redefinir senha",url,account.email)
    return {status:true, mensagem:"Enviamos uma solicitação de mudança de senha para o seu e-mail."}
  }catch(Exception){
    return {status:false, mensagem:"Email não cadastrado."}
  }

}
const recoveryEmail= async (req) => {
  const { body } = req
  console.log(body)
  let emailExists = await usuario.findOne({email:body.email}) ? true : false
  if(!emailExists){
    return {status:false, mensagem:"Email não cadastrado."}
  }
  try{
    let account = await usuario.findOne({email:body.email})
    let jwthash = account.email + "-" + new Date(account.createdAt).getTime()
    let token =  criptografia.criaTokenCustom({id:account._id},600000,jwthash)
    let url = `${process.env.FRONT_URL}/reset/email/${account._id}/${token}`
    console.log(url)
    await sendEmailRecovery("Mudança de e-mail","Redefinição de email",url,account.email)
    return {status:true, mensagem:"Enviamos uma solicitação de mudança de email para o seu e-mail."}
  }catch(Exception){
    return {status:false, mensagem:"Email não cadastrado."}
  }

}
const resetEmail = async (req) => {
  const { body  } = req
  console.log('reset email',body)
  try{
    let account = await usuario.findByIdAndUpdate(req.userId)
    account.email = body['email']
    await account.save()
    return {status:true, mensagem:"Seu email foi alterada com sucesso."}
  }catch(Exception){
    return {status:false, mensagem:"Ocorreu um erro ao mudar o email. Tente novamente."}
  }
}
const resetPassword = async (req) => {
  const { body  } = req
  try{
    let account = await usuario.findByIdAndUpdate(req.userId)
    account.senha = await criptografia.criaHash(body.senha)
    await account.save()
    return {status:true, mensagem:"Sua senha foi alterada com sucesso."}
  }catch(Exception){
    return {status:false, mensagem:"Ocorreu um erro ao mudar a senha. Tente novamente."}
  }
}
const criaConveniado = async (body) => {
  // return usuario.create({
  //   email: body.email,
  //   cargo: "60a4f5b1bd9f8d0fe1d45001",
  //   servico:body.servico,
  //   estabelecimento: body.estabelecimento,
  //   nome:body.nome,
  //   cpf:body.cpf,
  //   senha: md5(`${body.senha}${process.env.MD5_SECRET}`)
  // });

}
const del = async (_id) => {
  await usuario.deleteOne({_id});
}

module.exports = {
  autenticar,
  edit,
  lista,
  resetPassword,
  recoveryEmail,
  del,
  resetEmail,
  recoveryPassword,
  criaConveniado,
  cria,
}
