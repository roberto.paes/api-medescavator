const htmlStatus = (action,text) =>{ return `
<p>&nbsp;</p>
<p></p>
<!-- start preheader -->
<div class="preheader" style="display: none; max-width: 0; max-height: 0; overflow: hidden; font-size: 1px; line-height: 1px; color: #fff; opacity: 0;">A preheader is the short summary text that follows the subject line when an email is viewed in the inbox.</div>
<!-- end preheader -->
<p>&nbsp;</p>
<!-- start body -->
<table border="0" width="100%" cellspacing="0" cellpadding="0"><!-- start logo -->
<tbody>
<tr>
<td align="center" bgcolor="#e9ecef"><!-- [if (gte mso 9)|(IE)]>
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
        <tr>
        <td align="center" valign="top" width="600">
        <![endif]-->
<table style="max-width: 600px;" border="0" width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="padding: 36px 24px;" align="center" valign="top"><a style="display: inline-block;" href="https://sendgrid.com" target="_blank"> <img style="display: block; width: 48px; max-width: 48px; min-width: 48px;" src="./img/paste-logo-light@2x.png" alt="Logo" width="48" border="0" /> </a></td>
</tr>
</tbody>
</table>
<!-- [if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]--></td>
</tr>
<!-- end logo --> <!-- start hero -->
<tr>
<td align="center" bgcolor="#e9ecef"><!-- [if (gte mso 9)|(IE)]>
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
        <tr>
        <td align="center" valign="top" width="600">
        <![endif]-->
<table style="max-width: 600px;" border="0" width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="padding: 36px 24px 0; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; border-top: 3px solid #d4dadf;" align="left" bgcolor="#ffffff">
<h1 style="margin: 0; font-size: 32px; font-weight: bold; letter-spacing: -1px; line-height: 48px;">${action}</h1>
</td>
</tr>
</tbody>
</table>
<!-- [if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]--></td>
</tr>
<!-- end hero --> <!-- start copy block -->
<tr>
<td align="center" bgcolor="#e9ecef"><!-- [if (gte mso 9)|(IE)]>
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
        <tr>
        <td align="center" valign="top" width="600">
        <![endif]-->
<table style="max-width: 600px;" border="0" width="100%" cellspacing="0" cellpadding="0"><!-- start copy -->
<tbody>
<tr>
<td style="padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;" align="left" bgcolor="#ffffff">
<p style="margin: 0;">${text}</p>
</td>
</tr>
<tr>
<td align="left" bgcolor="#ffffff">&nbsp;</td>
</tr>
<tr>
<td style="padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; border-bottom: 3px solid #d4dadf;" align="left" bgcolor="#ffffff">
<p style="margin: 0;">${process.env.PROJECT_NAME}</p>
</td>
</tr>
</tbody>
</table>
<!-- [if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]--></td>
</tr>
<!-- end copy block --> <!-- start footer -->
<tr>
<td style="padding: 24px;" align="center" bgcolor="#e9ecef"><!-- [if (gte mso 9)|(IE)]>
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
        <tr>
        <td align="center" valign="top" width="600">
        <![endif]-->
<table style="max-width: 600px;" border="0" width="100%" cellspacing="0" cellpadding="0"><!-- start permission -->
<tbody>
<tr>
<td style="padding: 12px 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: #666;" align="center" bgcolor="#e9ecef">
<p style="margin: 0;">Voc&ecirc; recebeu este e-mail porque recebemos uma solicita&ccedil;&atilde;o de {action} para sua conta. Se voc&ecirc; n&atilde;o solicitou {action} pode excluir este e-mail com seguran&ccedil;a.</p>
</td>
</tr>
</tbody>
</table>
<!-- [if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]--></td>
</tr>
<!-- end footer --></tbody>
</table>
<!-- end body -->
`
}
module.exports = {
    htmlStatus
}