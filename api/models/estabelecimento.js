
const { Schema } = require("mongoose");

const EstabelecimentoSchema = {
  nome: {
    type: String,
    required: true,
  },
  descricao: {
    type: String,
    required: false,
  },
  contato: {
    type: [],
    required: false,
  },
  estrelas: {
    type: Number,
    required: false,
  },
  cnpj:{
    type: String,
    required: true,
  },
  status: {
    type: Number,
    required: false,
  },
  endereco:{
    logradouro: {
      type: String,
      required: false,
    },
    cep: {
      type: String,
      required: false,
    },
    complemento:{
      type: String,
      required: false,
    },
    estado: {
      type: String,
      required: false,
    },
    estadoSigla:{
      type: String,
      required: false,
    },
    municipio: {
      type: String,
      required: false,
    },
    bairro: {
      type: String,
      required: false,
    },
    numero: {
      type: String,
      required: false,
    }
  },
  imagem: {
    type : [],
    required:false
  },
  rank: {
    type: Number,
    default: 0,
    required: false
  },
  usuario: {type: Schema.Types.ObjectId, ref:'usuario',required: true}

}


module.exports = EstabelecimentoSchema;