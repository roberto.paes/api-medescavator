const { servico,estabelecimento, produto,plano, cargo, usuario } = require('../models/index');
const fileUtils = require('../utils/file.utils')
const {tagsGenerator} = require('../utils/search-engine')
//NOVO
const { requirePermission } = require('../utils/middlewares/permission.middleware')
const {  sendEmailStatus } = require('../utils/nodemailer')

const { endereco } = require('../models/estabelecimento');
const { deleteFile,updateFile } = require('../utils/s3')

const lista = async (_id,req) => {
  if(_id){
    return estabelecimento.findOne({_id})
  }
  return estabelecimento.find(req.finder)
}
const cria = async (model,req) => {
  let imagesTest = []
  if(model['cep']){
    model['cep'] = model['cep'].replace(/[^\d]/g, "");
  }
  if(model['cnpj']){
    model['cnpj'] = model['cnpj'].replace(/[^\d]/g, "");
  }
  let contato = []
  if(Array.isArray(model['contato'])){
    model['contato'].map((item)=>{
      contato.push(JSON.parse(item))
    })
    model['contato'] = contato
  }else if(model['contato']){
    model['contato'] = JSON.parse(model['contato'])
  }
  if(model.imagens){
    // console.log('body imagem')
    model.imagens.map((image)=>{
      imagesTest.push({nomeOriginal:image.originalname,name:image.filename,tipo:image.mimetype,alt:''})
    })
  }
  await estabelecimento.create({
    contato: model.contato,
    nome:model.nome,
    descricao:model.descricao,
    endereco:{
      cep:model.cep,
      complemento:model.complemento,
      logradouro:model.logradouro,
      estado:model.estado,
      municipio:model.municipio,
      bairro:model.bairro,
      numero:model.numero
    },
    status:model.status,
    cnpj:model.cnpj,
    rank:req.rank,
    usuario:req.userId,
    estrelas:model.estrelas,
    imagem:imagesTest
  });

  return {
    sucesso: true,
    mensagem: 'Loja cadastrada com sucesso'
  }

}
const edit = async (id,req) => {
  var update = {...req.body}
  let model = {...req.body}

  if(update['cep']){
    update['cep'] = update['cep'].replace(/[^\d]/g, "");
  }
  if(update['cnpj']){
    update['cnpj'] = update['cnpj'].replace(/[^\d]/g, "");
  }

  delete update['imagem']
  delete update['contato']
  var doc = await estabelecimento.findOneAndUpdate(req.finder, update,{
    useFindAndModify: false
  })

  if(requirePermission(["DISABLE_ESTABLISHMENT"],req)){
    let usuarioFinder = await usuario.findById(doc.usuario)
    let planoName = await plano.findById(usuarioFinder.plano)
    let cargoName = await cargo.findById(usuarioFinder.cargo)
    let text

    if(update['status'] == 0){
      update['status'] = 2
        if(planoName){
        text = `Olá, ${usuarioFinder.nome}. Obrigado por fazer parte do ${process.env.PROJECT_NAME} e ser nosso ${cargoName.name}.
        Sua loja ${doc.nome} foi desativada e seu plano " ${planoName.nome} " foi congelado. Agradecemos a compreensão. 💔
        `
      }else{
        text = `Olá, ${usuarioFinder.nome}.
        Obrigado por fazer parte do ${process.env.PROJECT_NAME}.
        Sua loja ${doc.nome} foi desativada.
         Agradecemos a compreensão. <3
        `
      }
      sendEmailStatus("Status de loja","[IMPORTANTE]Sobre sua loja " + process.env.PROJECT_NAME,text,usuarioFinder.email)
    }else if(update['status'] == 1){
      if(planoName){
        text = `Olá, ${usuarioFinder.nome}. Seja-bem vindo ao ${process.env.PROJECT_NAME} e ser nosso ${cargoName.name}.
        Somos uma plataforma suuuper planejada para o desenvolvimento da sua loja.
        Sua loja " ${doc.nome} " foi ativada e seu plano " ${planoName.nome} " foi ativado.
        Agradecemos a preferência. <3
        `
      }else{
        text = `Olá, ${usuarioFinder.nome}. Seja-bem vindo ao ${process.env.PROJECT_NAME} e ser nosso ${cargoName.name}.
        Somos Uma plataforma suuuper planejada para o desenvolvimento da sua loja.
        Sua loja ${doc.nome} foi desativada
        Agradecemos a preferência. <3
        `
      }
      sendEmailStatus("Status de loja","Sobre sua loja " + process.env.PROJECT_NAME,text,usuarioFinder.email)
    }
  }else{
    update['rank'] = req.rank
  }
  if(!doc){
    return false
  }
  let includeImage = model.includeImage.map((i)=>{
    return i.filename
  })
  updateFile(doc.imagem,includeImage,'estabelecimento')
  model.includeImage.map((item,index)=>{
    if(!item.filename){
      model.imagens[index] = {...model.imagens[index],...item}
    }else{
      model.imagens[index] = {...model.imagens[index],...item}
    }

  })

  if(model.imagens){
    let imagesTest = []
    model.imagens.map((image)=>{
      if(image.mimetype){
        imagesTest.push({nomeOriginal:image.originalname,name:image.filename,tipo:image.mimetype,alt:image.alt || ''})
      }else{
        imagesTest.push({nomeOriginal:image.originalname,name:image.filename,alt:image.alt || ''})
      }
    })
    doc.imagem = imagesTest
  }


  let contato = []
  if(Array.isArray(model['contato'])){
    model['contato'].map((item)=>{
      contato.push(JSON.parse(item))
    })
    doc['contato'] = contato
  }else if(model['contato']){
    console.log('problema aqui')
    console.log(JSON.parse(model['contato']))
    doc['contato'] = JSON.parse(model['contato'])
  }

  if(model.includeImage.length == 0){
    doc['imagem'] = undefined
  }
  if(!model['contato']){
    doc['contato'] = undefined
  }

  var newendereco = {
    cep:model.cep,
    complemento:model.complemento,
    logradouro:model.logradouro,
    estadoSigla:model.estadoSigla,
    estado:model.estado,
    municipio:model.municipio,
    bairro:model.bairro,
    numero:model.numero,
  }
  for(let prop in newendereco){
    if(!newendereco[prop]){
      delete newendereco[prop];
    }
  }

  doc.endereco = {...doc.endereco,...newendereco}
  await doc.save()
  return true
}
const finder = (req) => {

  var searchParams = req.query.query.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toUpperCase().split(' ');
  //let teste = []

  var regex = searchParams.map((item)=>{
    //teste.push({$regex:`^${item}`, $options : 'i'})
    return new RegExp( '^'+ item,'i' );
  })
  //return estabelecimento.find({ tags: { $all: searchParams } }, function (e, docs) {
  return estabelecimento.find({ tags: { $all :regex } }, function (e, docs) {
    return docs
  });
}

const del = async (req) => {
  const { _id } = req.finder
  var info = await estabelecimento.findOne(req.finder)
  if(!info){
    return false
  }
  if(info.imagem){
    info.imagem.map((image)=>{
      deleteFile(image.name,'estabelecimento')
    })
  }
  let produtos = await produto.find({estabelecimento:_id});
  await Promise.all(produtos.map(async (item) =>{
    item.imagem.map((image)=>{
      deleteFile(image.name,'produto')
    })
    await produto.findByIdAndDelete(item._id)
  }))
  await estabelecimento.deleteOne({_id});
  return true
}

module.exports = {
  cria,
  finder,
  lista,
  edit,
  del
}
