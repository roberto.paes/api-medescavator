// const mogoose = require('mongoose');
const { Schema } = require("mongoose");

const produtoSchema = {

  nome: {
    type: String,
    required: true,
  },
  preco: {
    type: Number,
    required: true,
  },
  curtida: {
    type: Number,
    required: false,
    default : 0,
  },
  descricao: {
    type: String,
    required: true,
  },
  status: {
    type: Boolean,
    default : true,
    required: false,
  },
  modelo: {
    type: String,
    required: false,
  },
  fabricante: {
    type: String,
    required: false,
  },
  tags: {
    type: [String],
    required: false,
  },
  imagem: {
    type : [],
    required:false
  },
  usuarioVotante: [{ type : Schema.ObjectId, ref: 'usuario', required:false }],

  categorias: [{ type : Schema.ObjectId, ref: 'categoria',required:false }],

  estabelecimento: {type: Schema.ObjectId, ref:'estabelecimento',required:true}
}


module.exports = produtoSchema;



