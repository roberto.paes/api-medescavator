const cargoService = require('../services/cargo.service');


const createCargo= async (req,res,next) => {
  cargoService.cria(req.body)
  console.log('req.body',req.body)
  return res.status(200).send({
    mensagem: "Cargo registrado com sucesso."
  });
}

const delCargo = (req, res, next) => {
  cargoService.del(req.body)
  return res.status(200).send({
    mensagem: "Cargo deletado com sucesso."
  });

}
const getAllCargo = async (req, res, next) => {
  let cargos =  await cargoService.getAll()
  return res.status(200).json(cargos)
}
const editCargo = async (req,res,next) => {
  cargoService.edit(req.body)

  return res.status(200).send({
    mensagem: "Cargo alterado com sucesso."
  });
}

module.exports = {
  createCargo,
  delCargo,
  getAllCargo,
  editCargo
}