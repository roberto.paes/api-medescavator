const express = require('express');
const cors = require('cors');
const mongoose = require("mongoose");
const path = require('path');

const db = require('../db/config');
const root_dir  = path.join(path.dirname(require.main.filename),'arquivos')

mongoose.connect(db.uri, {useUnifiedTopology: true,useNewUrlParser: true,useFindAndModify:false});

const app = express();
app.use('/img', express.static(root_dir));
console.log('serving images in ',root_dir)
app.use(express.json());
app.use(cors());

const router = require('./routes/index');
router(app);

const port = 8000

app.listen(port, () => {
});


module.exports = app;


