// const mogoose = require('mongoose');
const { Schema } = require("mongoose");


const permissionSchema = {
  _id: {
    type: Schema.Types.ObjectId,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  permission: {
    type: Array,
    required: true,
  }
}


module.exports = permissionSchema;



