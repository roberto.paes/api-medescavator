const usuarioService = require('../services/usuario.service')
const { sendResponse } = require('../utils/response.utils')

const auth = async (req, res, next) => {
  const { email, senha } = req.body;
  const resultadoServico = await usuarioService.autenticar(email, senha);
  const codigoRetorno = resultadoServico.sucesso ? 200 : 401;
  const dadoRetorno = resultadoServico.sucesso ? { data: resultadoServico.data } : { detalhes: resultadoServico.detalhes };

  return res.status(codigoRetorno).send({
    mensagem: resultadoServico.mensagem,
    ...dadoRetorno
  });

}
const createUser = async (req,res,next) => {
  let result = await usuarioService.cria(req.body)
  if(result.status){
    return res.status(200).send({
      mensagem: result.mensagem
    });
  }else{
    return res.status(404).send({
      mensagem: result.mensagem
    });
  }
}
const lista = async (req,res,next) => {
  let response = await usuarioService.lista(req)
  //return response
  return res.status(200).json(response);
  // console.log('doc####',doc)
  // return sendResponse(res,200,[response,"Ocorreu um erro ao listar o usuário."],true)
}
const editUser = async (req,res,next) => {
  let response = await usuarioService.edit(req)
  //return response
  //console.log('doc####',doc)
  return sendResponse(res,200,["Usuário atualizado com sucesso","Ocorreu um erro ao atualizar o usuário."],response)
}
const createConveniado = async (req,res,next) => {

  usuarioService.criaConveniado(req.body)
  return res.status(200).send({
    mensagem: "Registrado com sucesso."
  });
}
const delUser =  async (req, res, next) => {
  await usuarioService.del(req.params.id)
  return res.status(200).send({
    mensagem: "Usuario deletado com sucesso."
  });

}

const recoveryPassword =  async (req, res, next) => {
  let result = await usuarioService.recoveryPassword(req)
  if(result.status){
    return res.status(200).send({
      mensagem: result.mensagem
    });
  }else{
    return res.status(404).send({
      mensagem: result.mensagem
    });
  }
}
const recoveryEmail =  async (req, res, next) => {
  let result = await usuarioService.recoveryEmail(req)
  if(result.status){
    return res.status(200).send({
      mensagem: result.mensagem
    });
  }else{
    return res.status(404).send({
      mensagem: result.mensagem
    });
  }
}
const resetEmail =  async (req, res, next) => {
  let result = await usuarioService.resetEmail(req)
  if(result.status){
    return res.status(200).send({
      mensagem: result.mensagem
    });
  }else{
    return res.status(404).send({
      mensagem: result.mensagem
    });
  }
}
const resetPassword =  async (req, res, next) => {
  let result = await usuarioService.resetPassword(req)
  if(result.status){
    return res.status(200).send({
      mensagem: result.mensagem
    });
  }else{
    return res.status(404).send({
      mensagem: result.mensagem
    });
  }
}
module.exports = {
  auth,
  editUser,
  resetPassword,
  recoveryPassword,
  resetEmail,
  recoveryEmail,
  delUser,
  lista,
  createConveniado,
  createUser
}