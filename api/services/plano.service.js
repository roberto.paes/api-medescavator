const { plano } = require('../models/index');
const cria = async (body) => {
  return plano.create(body);
}
const get = async (req) => {
  const { id } = req.params
  if(id){
    return await plano.findOne({_id:id})
  }
  return await plano.find({})
}
const edit = async (req) => {
  let body = {...req.body}
  const { id } = req.params
  return plano.updateOne({_id:id}, body);
}
const del = async (req) => {
  let body = {...req.body}
  const { id } = req.params
  return plano.deleteOne({_id:id});
}

module.exports = {
  cria,
  del,
  get,
  edit
}
