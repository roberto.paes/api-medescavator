
const cargoController = require('../../controllers/cargo.controller');
const validaDTO = require('../../utils/middlewares/validate-dto.middleware');
const {requirePermissionsMiddleware,verifyJWT} = require('../../utils/middlewares/permission.middleware')



module.exports = (router) => {
  router.route('/cargo')
  // .post(verifyJWT,requirePermissions(["CREATE_CARGO"]),
  //   cargoController.createCargo
  // )
  // .put(verifyJWT,requirePermissions(["EDIT_CARGO"]),
  //   cargoController.editCargo
  // )
  // .delete(verifyJWT,requirePermissions(["DELETE_CARGO"]),
  // cargoController.delCargo
  // )VIEW_ALL_CARGO
  // .post(verifyJWT,requirePermissionsMiddleware(["XXXXXXXXXX"]),
  //   cargoController.createCargo
  //   )
    .get(verifyJWT,requirePermissionsMiddleware(["VIEW_ALL_CARGO"]),
    cargoController.getAllCargo
    )
  // .post(
  //   cargoController.createCargo
  //   )
  //   .put(
  //     cargoController.editCargo
  //     )
  //     .delete(
  //       cargoController.delCargo
  //       )

      }