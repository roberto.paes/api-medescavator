const { Seeder } = require('mongoose-data-seed')
const { cargo } = require('../api/models/index')

var data = [
  {
    "_id":"222222222222222222222222",
    "permission": ["EDIT_USER","DELETE_USER","VIEW_ALL_CARGO", "CREATE_COMMENT","EDIT_COMMENT","DELETE_COMMENT","IGNORE_EXTRA","VIEW_USER","VIEW_ALL_ESTABLISHMENT","VIEW_ALL_CATEGORY","DISABLE_ESTABLISHMENT","DISABLE_USER","VIEW_ALL_CARGO","VIEW_ALL_PRODUCT","VIEW_ALL_USER" ,"EDIT_CARGO", "CREATE_CARGO", "DELETE_CARGO", "EDIT_CATEGORY", "CREATE_CATEGORY", "CREATE_ESTABLISHMENT", "CREATE_PRODUCT", "DELETE_PRODUCT", "EDIT_PRODUCT", "DELETE_CATEGORY", "VIEW_CATEGORY", "VIEW_LOJA","VIEW_PRODUCT", "EDIT_ESTABLISHMENT", "DELETE_ESTABLISHMENT"],
    "name": "Administrador",
    "description": "Administrador. Controle total.",
  },
  {
    "_id":"111111111111111111111111",
    "permission": [ "CREATE_ESTABLISHMENT", "EDIT_ESTABLISHMENT", "DELETE_ESTABLISHMENT", "CREATE_PRODUCT", "DELETE_PRODUCT", "EDIT_PRODUCT", "VIEW_LOJA","VIEW_PRODUCT"],
    "name": "Lojista",
    "description": "Lojista. Administra produtos e administra suas lojas.",
  },
  {
    "_id":"000000000000000000000000",
    "permission": ["SKIP_PAINEL","CREATE_COMMENT","EDIT_COMMENT","DELETE_COMMENT"],
    "name": "Cliente",
    "description": "Cliente. Efetuar busca por produtos e vota nos melhores produtos.",
  }
]

class cargoSeeder extends Seeder {
  async shouldRun() {
    return cargo.countDocuments()
    .exec()
    .then(count => count === 0);
  }

  async run() {
    return cargo.create(data);
  }
}

module.exports = {
  cargoSeeder
}
