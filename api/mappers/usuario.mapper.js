const trataTipoUsuario = (tipo) => {

  switch (tipo) {
    case "administrador":
    return 1;

    default:
    break;
  }

}

const toCargoDTO = (model,extra) => {

  let { name, description, permission } = model;
  if(extra){
    permission = permission.concat(extra)
  }
  return {
    name,
    permission,
    description
  }
}


const toEstDTO = (model) => {

  let { _id } = model;
  return {
    _id
  }
}
const toUserDTO = (model) => {

  const { id, email, kind, nome, nomeFantasia,cargo } = model;


  return {
    id,
    email,
    cargo,
    nome: nome ? nome : nomeFantasia,
    tipoUsuario: trataTipoUsuario(kind),
  }

}


module.exports = {
  toUserDTO,
  toCargoDTO,
  toEstDTO
}


