const { Schema } = require("mongoose");

module.exports = {
  nome: {
    type: String,
    required: true,
  },
  descricao: {
    type: String,
    required: false,
  },
  status: {
    type: Boolean,
    required: true,
  },
  index: {
    type: Number,
    required: false,
  },
  global: {
    type: Boolean,
    required: false,
  },
  categorias: [{ type : Schema.ObjectId, ref: 'categoria', required:false }]


}
