
const tagsGenerator = (model,toTags) =>{
    const regex = /[\s]/
    let tags = []
    // if(!Array.isArray(toTags)){
    //     return
    // }
    toTags.map((item)=>{
        if(model[item]){
            if(Array.isArray(model[item])){
                model[item].map((item)=>{
                    if(item.length !== 0){
                        tags.push(...item.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toUpperCase().trim().split(regex))
                    }
                })

            }else{
                if(item.length !== 0){
                    tags.push(...model[item].normalize('NFD').replace(/[\u0300-\u036f]/g, "").toUpperCase().trim().split(regex))
                }
            }
        }
    })
    return tags
}
module.exports = {
    tagsGenerator
}