const { produto,estabelecimento } = require('../models/index');
const {updateTags,removeTags,generateAllTags} = require('../utils/estabelecimento.utils')
const {tagsEditorMiddleware} = require('../utils/middlewares/produto/tags.middleware')
const fileUtils = require('../utils/file.utils')
const { deleteFile,updateFile } = require('../utils/s3')
const { Types } = require("mongoose");

const cria = async (req) => {
  const body = {...req.body}
  let imagesTest = []
  let categorias = []

  if(Array.isArray(body['categorias'])){
    body['categorias'].map((item)=>{
      categorias.push(item)
    })
    body['categorias'] = categorias
  }else if(body['categorias']){
    body['categorias'] = body['categorias']
  }
  if(body.includeImage){
    body.includeImage.map((item,index)=>{
      if(!item.filename){
        body.imagens[index] = {...body.imagens[index],...item}
      }else{
        body.imagens[index] = {...body.imagens[index],...item}
      }
    })
    if(body.imagens){
      body.imagens.map((image)=>{
        imagesTest.push({nomeOriginal:image.originalname,name:image.filename,tipo:image.mimetype,alt:image.alt || ''})
      })
    }
  }
  // delete update['categorias']
  await produto.create({...body,imagem:imagesTest});
}

function insertAt(array, index, ...elementsArray) {
  array.splice(index, 0, ...elementsArray)
}
const edit = async (_id,req) => {
  var update = {...req.body}
  let model = {...req.body}
  delete update['imagem']
  delete update['categorias']

  var doc = await produto.findOneAndUpdate(req.finder, update,{
    useFindAndModify: false
  })
  //console.log('find',doc)
  if(!doc){
    return false
  }
  let includeImage = model.includeImage.map((i)=>{
    return i.filename
  })
  updateFile(doc.imagem,includeImage,'produto')
  model.includeImage.map((item,index)=>{
    if(!item.filename){
      model.imagens[index] = {...model.imagens[index],...item}
    }else{
      model.imagens[index] = {...model.imagens[index],...item}
    }
  })
  if(model.imagens){
    let imagesTest = []
    model.imagens.map((image)=>{
      if(image.mimetype){
        imagesTest.push({nomeOriginal:image.originalname,name:image.filename,tipo:image.mimetype,alt:image.alt || ''})
      }else{
        imagesTest.push({nomeOriginal:image.originalname,name:image.filename,alt:image.alt || ''})
      }
    })
    doc.imagem = imagesTest
  }
  let categorias = []
  if(Array.isArray(model['categorias'])){
    model['categorias'].map((item)=>{
      categorias.push(item)
    })
    doc['categorias'] = categorias
  }else if(model['categorias']){
    doc['categorias'] = model['categorias']
  }

  if(model.includeImage.length == 0){
    doc['imagem'] = undefined
  }
  if(!model['categorias']){
    doc['categorias'] = undefined
  }

  await doc.save()
  return true

}
const del = async (_id) => {
  // await removeTags(body)
  var info = await produto.findById(_id)
  if(info.imagem){
    info.imagem.map((image)=>{
      deleteFile(image.name,'produto')
    })
  }
  await produto.deleteOne({_id});
  // await estabelecimento.updateOne({_id:doc.estabelecimento}, {
  //   tags:await generateAllTags({id:doc.estabelecimento})
  // });

}

const lista = async (_id,req) => {
  if(_id){
    return produto.findOne({_id}).populate("estabelecimento");
  }
  return produto.find({}).populate("estabelecimento").populate('categorias');
}
const listaProdutoCategoria = async (req) => {
  let { page = 1, limit=5, price = 0, relevante = 0} = req.query
  relevante = parseInt(relevante)
  price = parseInt(price)
  let sortArray = [["curtida", relevante],["preco", price],["estabelecimento.rank", -1]]
  if([1,-1].includes(price) == false){
    let index =  sortArray.findIndex((item)=> item[0] === 'preco')
    sortArray.splice(index,1)
  }
  if([1,-1].includes(relevante) == false){
    let index =  sortArray.findIndex((item)=> item[0] === 'curtida')
    sortArray.splice(index,1)
  }
  let objectFilter = {}
  sortArray.map((item,i)=>{
    objectFilter[item[0]] = sortArray[i][1]
  })
  console.log(objectFilter)
  let json_finder = {categorias:req.params.id,status:true}
  var objectId = Types.ObjectId(req.params.id)
  let aggregateQuery = [
    {$lookup:{from:"estabelecimentoCollection",localField:"estabelecimento", foreignField:"_id", as:"estabelecimento"}},
    {$lookup:{from:"categoriaCollection",localField:"categorias", foreignField:"_id", as:"categorias"}},
    { $unwind: '$estabelecimento' },
    {
      $match: {
        "estabelecimento.status": 1
      }
    },
    {
      $match: {
        "categorias._id": objectId
      }
    }
  ]
  function products() {
    return new Promise((resolve, reject) =>{
      if(sortArray.length > 0 ){
        aggregateQuery.push( { $sort: objectFilter })
      }
      aggregateQuery.push({ $skip: (page - 1)* limit})
      aggregateQuery.push({ $limit: limit * 1 })

      produto.aggregate(aggregateQuery).exec(function (err, docs) {
        err ? reject(err) : resolve(docs)
      })
    } )
  }
  function productsCount() {
    return new Promise((resolve, reject) =>
    produto.find(json_finder)
    .populate("estabelecimento")
    .populate('categorias')
    .exec(function (err, docs) {
      let result =  docs.filter(function(doc){
        return ![2,0].includes(doc.estabelecimento.status)
      })
      err ? reject(err) : resolve(result.length)
    })
    );
  }
  let calcPages
  let totalPage = 0
  let totalProducts = 0

  await productsCount().then(function(data) {
    calcPages = data % limit
    totalProducts = data

    if(calcPages != 0 ){
      totalPage++
    }
    totalPage += Math.trunc(data / limit)
  })
  return await products().then(function(products) {
    return { currentPage: parseInt(page),totalPage,totalProducts,total:products.length,products }
  });
  //produto.find({categorias:id}).populate("estabelecimento").populate('categorias')
}
const listaAvancada = async (params,value) => {
  if(params){
    return produto.find({[params]:value})
  }
  return produto.find({})
}
const finder = async (req) => {
  let { page = 1, limit=5, price = 0, relevante = 0, query = ''} = req.query
  relevante = parseInt(relevante)
  price = parseInt(price)
  var searchParams = query.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toUpperCase().split(' ')
  let sortArray = [["curtida", relevante],["preco", price],["estabelecimento.rank", -1]]
  if([1,-1].includes(price) == false){
    let index =  sortArray.findIndex((item)=> item[0] == "preco")
    sortArray.splice(index,1)
  }
  if([1,-1].includes(relevante) == false){
    let index =  sortArray.findIndex((item)=> item[0] == "curtida")
    sortArray.splice(index,1)
  }
  let objectFilter = {}
  sortArray.map((item,i)=>{
    objectFilter[item[0]] = sortArray[i][1]
  })
  var regex = searchParams.map((item)=>{
    return new RegExp( '^'+ item,'i' );
  })
  let json_finder = { tags: { $all :regex },status:true}
  let aggregateQuery = [
    {$lookup:{from:"estabelecimentoCollection",localField:"estabelecimento", foreignField:"_id", as:"estabelecimento"}},
    {$lookup:{from:"categoriaCollection",localField:"categorias", foreignField:"_id", as:"categorias"}},
    { $unwind: '$estabelecimento' },
    {
      $match: {
        "estabelecimento.status": 1
      }
    },
    {
      $match: {
        tags: { $all :regex },
        status: true
      }
    }
  ]
  function products() {
    return new Promise((resolve, reject) =>{
      if(sortArray.length > 0 ){
        aggregateQuery.push( { $sort: objectFilter })
      }
      aggregateQuery.push({ $skip: (page - 1)* limit})
      aggregateQuery.push({ $limit: limit * 1 })
      produto.aggregate(aggregateQuery).exec(function (err, docs) {
        err ? reject(err) : resolve(docs)
      })
    })
  }


  function productsCount() {
    return new Promise((resolve, reject) =>
    produto.find(json_finder)
    .populate("estabelecimento")
    .populate('categorias')
    .exec(function (err, docs) {
      let result =  docs.filter(function(doc){
        return ![2,0].includes(doc.estabelecimento.status)
      })
      err ? reject(err) : resolve(result.length)
    })
    );
  }
  let calcPages
  let totalPage = 0
  let totalProducts = 0
  await productsCount().then(function(data) {
    calcPages = data % limit
    totalProducts = data
    if(calcPages != 0 ){
      totalPage++
    }
    totalPage += Math.trunc(data / limit)//ignore
  })
  return products().then(function(products) {
    return { currentPage: parseInt(page),totalPage,totalProducts,total:products.length,products }
  });

}
module.exports = {
  cria,
  finder,
  listaProdutoCategoria,
  listaAvancada,
  lista,
  del,
  edit
}
