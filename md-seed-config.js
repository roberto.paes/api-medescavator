const mongoose = require("mongoose")
const {cargoSeeder} = require("./seeders/cargo")
const {planoSeeder} = require("./seeders/plano")

const mongoURL = `mongodb://${process.env.MONGO_HOST}:27017/${process.env.MONGO_DB_NAME}`

/**
 * Seeders List
 * order is important
 * @type {Object}
 */
 const seedersList = {
  cargoSeeder,
  planoSeeder
};
/**
 * Connect to mongodb implementation
 * @return {Promise}
 */
 const connect = async () =>
  await mongoose.connect(mongoURL, { useNewUrlParser: true });
/**
 * Drop/Clear the database implementation
 * @return {Promise}
 */
 const dropdb = async () => mongoose.connection.db.dropDatabase();

 module.exports = {
  connect,
  dropdb,
  seedersList
 }