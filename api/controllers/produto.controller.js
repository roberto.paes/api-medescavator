const Produto = require('../services/produto.service');
const {tagsUpdateMiddleware} = require('../utils/middlewares/produto/tags.middleware')


const createProduto= async (req,res,next) => {
  console.log('creating produto',req.body)
  await Produto.cria(req)
  //console.log('req.body',req.body)
  return res.status(200).send({
    mensagem: "Produto registrado com sucesso."
  });
}
const advancedList = async (req,res,next) => {
  const lista = await Produto.listaAvancada(req.params.params,req.params.value)

  return res.status(200).send(lista)
}

const delProduto =  async (req, res, next) => {
  await Produto.del(req.params.id)
  return res.status(200).send({
    mensagem: "Produto deletado com sucesso."
  });

}
const lista = async (req,res,next) => {
  try {
    const produtos = await Produto.lista(req.params.id,req)
    return res.status(200).send(produtos);
  } catch (err) {

    return res.status(201).send(err);

  }
}
const listaProduto = async (req,res,next) => {
  const lista = await Produto.listaProdutoCategoria(req)

   return res.status(200).json(lista)
 }
const editProduto = async (req,res,next) => {
 let response = await Produto.edit(req.params.id,req)
 // console.log('doc####',doc)
 await tagsUpdateMiddleware('produto',req)

 if(response){
  return res.status(200).send({
    mensagem: "produto editado com sucesso."
  });
}else{
  return res.status(404).send({
    mensagem: "Ocorreu algum erro ao deletar esse produto."
  });
}
}
const search = async (req,res,next) =>{
  const result = await Produto.finder(req)
  //console.log('return',result)
  res.status(200).json(result)
}

module.exports = {
  createProduto,
  advancedList,
  delProduto,
  search,
  listaProduto,
  lista,
  editProduto
}