const jwt = require('jsonwebtoken');
const { usuario, estabelecimento  } = require('../../models/index');
const { validaPasswordChangeHash,validaEmailChangeHash } = require('../criptografia.utils')
const jwtHashSecret = process.env.JWT_SECRET;
function requirePermissionsMiddleware(permissions) {
    return function(req, res, next) {
        const usuarioPerm = req.permission
        var valid = false
        permissions.map((item) => {
            if(usuarioPerm.includes(item))
            {
                valid = true
            }else{
                valid = false
            }
        }
        )
        if(valid){
            next()
        }else{
            res.status(401).json({ status: false, mensagem: 'Você não tem as permissões corretas para executar isso.' });
        }
    }
}


function requirePermissionAdvanced(permissions,req) {
    const usuarioPerm = req
    if(!usuarioPerm){
        return false
    }
    let max = false
    let valid = false
    let ignoreExtra = false
    permissions.map((item) => {
        let check
        if(Array.isArray(item)){
            max = item[1]
            check = item[0].slice(-1)
          }else{
            check = item.slice(-1)
          }
        if(check == '_'){
            usuarioPerm.map((item)=>{
                let result = item.split(check)
                if(!max){
                    valid = result[1]
                }else{
                    if(result[1] <= max){
                        valid = result[1]
                    }
                }
            })
        }else{
            if(usuarioPerm.includes(item))
            {
                valid = true
            }else{
                valid = false
            }
        }
    })
    return valid
}

function requirePermission(permissions,req) {

    const usuarioPerm = req.permission
    if(!usuarioPerm){
        return false
    }
    var valid = false
    let ignoreExtra = false
    permissions.map((item) => {
        if(usuarioPerm.includes(item))
        {
            valid = true
        }else{
            valid = false
        }
    }
    )
    if(usuarioPerm.includes("IGNORE_EXTRA"))
    {
        ignoreExtra = true
    }else{
        ignoreExtra = false
    }
    if(ignoreExtra){
        valid = ignoreExtra
    }
    return valid
}
const databaseFinder = (route,permission) =>{
    return async function(req, res, next){
        let find = {}
        let _id = false
        if(req.params.id){
            _id = req.params.id
        }
        if(!req.permission && !req.userId){
            if(_id){
                find = {_id}
            }else{
                find = {}
            }
            req.finder = find
            return next()
        }
        switch(route){
            case 'produto':
            const estabelecimentos = await estabelecimento.find({usuario:req.userId})
            let estabelecimentoList = estabelecimentos.map((item)=>{
                return item._id
            })
            if(_id){
                if(req.permission.includes(permission)){
                    find = { _id }
                }else{
                    find = { _id, estabelecimento:estabelecimentoList }
                }
            }else{
                if(req.permission.includes(permission)){
                    find = {}
                }else{
                    find = { estabelecimento:estabelecimentoList }
                }
            }
            break
            default:
            if(_id){
                if(req.permission.includes(permission)){
                    find = { _id }
                }else{
                    find = { _id,usuario:req.userId,status:{$ne:3} }
                }
            }else{
                if(req.permission.includes(permission)){
                    find = {}
                }else{
                    find = { usuario:req.userId,status:{$ne:3} }
                }
            }
            break
        }

        req.finder = find
        next()
    }
}
const verifyJWTUpdateUser = (header,tipo) =>{
    return async function(req, res, next){
        const token = req.headers[header];
        if (!token) return res.status(403).json({ status: false, mensagem: 'Sem chave de acesso.' })
        let id = req.body.id
        let hash
        switch(tipo){
            case 'senha':
            hash = await validaPasswordChangeHash(id)
            break
            case 'email':
            hash = await validaEmailChangeHash(id)
            break
        }

        if(!hash){
            return res.status(403).json({ status: false, mensagem: 'Usuário não encontrado.' });
        }
        jwt.verify(token, hash, function(err, decoded) {
            if (err) return res.status(500).json({ status: false, mensagem: 'Falha ao autenticar token.' });
            req.userId = decoded.id
            next();
        });
    }
}
const verifyJWTOptional = function(req, res, next){
    const token = req.headers['x-access-token'];
    if (!token) return next()
    jwt.verify(token, jwtHashSecret, function(err, decoded) {
        if (err) return res.status(500).json({ status: false, mensagem: 'Falha ao autenticar token.' });

        //set user tipo and userid to routes
        req.permission = decoded.permission;
        req.userId = decoded.id;
        //console.log('user',req.permission, req.userId)
        console.log('token identificado')
        next();
    });
}
const verifyJWT = function(req, res, next){
    const token = req.headers['x-access-token']
    // console.log('received ', token)
    if (!token) return res.status(403).json({ status: false, mensagem: 'Sem chave de acesso.' })

    jwt.verify(token, jwtHashSecret, function(err, decoded) {
        if (err) return res.status(500).json({ status: false, mensagem: 'Falha ao autenticar token.' })

        //set user tipo and userid to routes
        req.permission = decoded.permission
        req.userId = decoded.id
        //console.log('user',req.permission, req.userId)
        // console.log('token identificado', req.permission)
        next();
    });
}
// const permissionVerify = (req,res,next) =>{
//     return permissionSet = (permission) =>{
//         console.log('pmerission',permission)
//     }
//   // const usuarioPerm = req.permission
//    //const validate = usuarioPerm.some(item => permission.includes(item))
// //console.log('validate permission',validate)
// next()
//  }


module.exports = {
    requirePermissionsMiddleware,
    verifyJWTOptional,
    requirePermission,
    databaseFinder,
    requirePermissionAdvanced,
    verifyJWTUpdateUser,
    verifyJWT
}