
const produtoController = require('../../controllers/produto.controller');
const validaDTO = require('../../utils/middlewares/validate-dto.middleware');
const {requirePermissionsMiddleware,verifyJWT,databaseFinder} = require('../../utils/middlewares/permission.middleware')
const {tagsGeneratorMiddleware} = require('../../utils/middlewares/produto/tags.middleware')
const fileUploadMiddlare = require('../../utils/middlewares/file-upload.middleware')
const {produtoMiddleware} = require('../../utils/middlewares/produto/produto.middleware')

module.exports = (router) => {
  router.route('/produto')
  .get(
    produtoController.lista
    )
    router.route('/produto/advanced/:params/:value').get(
      produtoController.advancedList
      )
      router.route('/produto/categoria/:id')
      .get(
        produtoController.listaProduto
        )
        router.route('/produto')
        .post(verifyJWT,requirePermissionsMiddleware(["CREATE_PRODUCT"]),produtoMiddleware,fileUploadMiddlare('produto'),tagsGeneratorMiddleware('produto',['nome','modelo','fabricante']),
        produtoController.createProduto
        )
        // router.route('/produto')
        // .post(verifyJWT,requirePermissionsMiddleware(["CREATE_PRODUCT"]),produtoMiddleware,fileUploadMiddlare('produto'),tagsGeneratorMiddleware('produto',['nome','modelo','fabricante']),
        // produtoController.createProduto
        // )
        router.route('/produto/:id')
        .put(verifyJWT,requirePermissionsMiddleware(["EDIT_PRODUCT"]),produtoMiddleware,databaseFinder('produto','VIEW_ALL_PRODUCT'),fileUploadMiddlare('produto'),
        produtoController.editProduto
        )
        router.route('/produto/:id')
        .delete(verifyJWT,requirePermissionsMiddleware(["DELETE_PRODUCT"]),databaseFinder('produto','VIEW_ALL_PRODUCT'),
        produtoController.delProduto
        )
        router.route('/produto/search')
        .get(
          produtoController.search
          )
          router.route('/produto/:id')
          .get(
            produtoController.lista
            )


          }