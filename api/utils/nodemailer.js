const nodemailer = require('nodemailer')
const emailUser = process.env.EMAILUSER
const emailPassword = process.env.EMAILPASSWORD
const emailHost = process.env.EMAILHOST
const {htmlRecovery} = require('../utils/email/recovery')
const {htmlStatus} = require('../utils/email/status')

let transporter = nodemailer.createTransport({
    name: emailUser,
    host: emailHost,
    port: 465,
    secure: true,
    service:emailHost,
    auth: {
        user:emailUser,
        pass:emailPassword
    }
},
)

async function sendEmailRecovery(action,subject,url,emails){
    await transporter.sendMail({
        html: htmlRecovery(action,subject,url),
        subject: `${subject} ${emailUser}`,
        from: `${subject} ${emailUser}`,
        to: emails
    })
}
async function sendEmailStatus(action,subject,text,emails){
    await transporter.sendMail({
        html: htmlStatus(action,text),
        subject: `${subject} ${emailUser}`,
        from: `${subject} ${emailUser}`,
        to: emails
    })
}

module.exports = {
    sendEmailRecovery,
    sendEmailStatus
}