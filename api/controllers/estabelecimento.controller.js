const estabelecimentoService = require('../services/estabelecimento.service');

const {tagsUpdateMiddleware} = require('../utils/middlewares/produto/tags.middleware')

const createEstabelecimento = (req, res, next) => {

  const { body } = req;
  console.log('chegando aqui')
  estabelecimentoService.cria(body,req)
  return res.status(200).send({
    mensagem: "estabelecimento registrada com sucesso."
  });

}
const editEstabelecimento = async (req, res, next) => {
  console.log('edit body',req.params.id)
 let response = await estabelecimentoService.edit(req.params.id,req)
 await tagsUpdateMiddleware('estabelecimento',req)

 if(response){
  return res.status(200).send({
    mensagem: "estabelecimento deletado com sucesso."
  });
}else{
  return res.status(404).send({
    mensagem: "Ocorreu algum erro ao deletar esse estabelecimento."
  });
}
}
const delEstabelecimento = async (req, res, next) => {
  console.log('delete',req.params.id)
let response = await estabelecimentoService.del(req)
if(response){
  return res.status(200).send({
    mensagem: "estabelecimento deletado com sucesso."
  });
}else{
  return res.status(404).send({
    mensagem: "Ocorreu algum erro ao deletar esse estabelecimento."
  });
}

}
const search = async (req,res,next) =>{
  var result = await estabelecimentoService.finder(req)
  //console.log('return',result)
  res.status(200).send(result)
}
const lista = async (req, res, next) => {

  try {
    const estabelecimentos = await estabelecimentoService.lista(req.params.id,req)
    return res.status(200).send(estabelecimentos);
  } catch (err) {

    return res.status(201).send(err);

  }
}


module.exports = {
  lista,
  search,
  createEstabelecimento,
  delEstabelecimento,
  editEstabelecimento
}