
const { getEstabelecimentoCount } = require('../../estabelecimento.utils')
const { requirePermission } = require('../permission.middleware')

async function produtoMiddleware (req, res, next) {

    let numberImage = 3
    if(requirePermission(['PROD_IMG_5'], req)){
        numberImage = 5
    }
    else if(requirePermission(['PROD_IMG_10'], req)){
        numberImage = 10
    }
    req.numberImage = numberImage

    // let rank = 0
    // if(requirePermission(['RANK_1'], req)){
    //     rank = 1
    // }
    // else if(requirePermission(['RANK_2'], req)){
    //     rank = 2
    // }
    // else if(requirePermission(['RANK_3'], req)){
    //     rank = 3
    // }
    // else if(requirePermission(['RANK_4'], req)){
    //     rank = 4
    // }
    // else if(requirePermission(['RANK_5'], req)){
    //     rank = 5
    // }

    // req.rank = rank

    next()
}

module.exports = {
    produtoMiddleware
}