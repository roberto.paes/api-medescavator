# Introdução

### Aluno: Roberto Carneiro Paes
### Projeto: BuscaPreço
### Curso: Bootcamp Desenvolvimento Full Stack (MERN) - INFNET
[Acessar repositório frontend](https://gitlab.com/roberto.paes/front-medescavator)

[Acessar repositório backend](https://gitlab.com/roberto.paes/api-medescavator)
# O que é?

### É uma plataforma de busca sintética de produtos em lojas cadastradas e com ranqueamento dinâmico.

# Por que usar a plataforma?

### É um ambiente seguro, o usuário tem certeza do comprometimento da loja e o lojista tem certeza que sua loja será vista.

# Essa plataforma tem um nome?

### Não exatamente. O nome atualmente é BuscaPreço, mas já há uma entidade com esse nome. Estamos abertos a sugestões :heart:

# Forma de lucro

### É possível obter renda financeira através da venda de planos aos lojistas. Cada plano oferece uma experiência diferente ao lojista. Dando possibilidade de escalar quantidades de loja, quantidade de imagens por produto ou aumento do ranqueamento da loja. Ou qualquer outra coisa que seja passível de limitação.

<div class="page"/>

# Fluxograma

<img src="./fluxoprojeto.png"></img>


<div class="page"/>


# Visão geral de recursos

1. Administração de perfil: Incluindo mudança de imagem do avatar, mudança de senha e mudança de e-mail.
Busca multi dimensional por produtos
Comentários em produtos para classificar a qualidade do produto ou lojista.

2. Filtro por relevância e preço
Listagem de comentários com filtros de relevância e data
Ranqueamento de produto baseado na opinião dos usuários e no plano contratado pelo lojista.

3. Recuperação de senha para usuários através de chave de acesso.
Usuários podem comentar sua satisfação uma única vez e também podem votar uma única vez em outros comentários.
Lojistas podem contratar serviços extras, de pagamento mensal e que podem escalar de acordo com a necessidade do sistema.
O administrador pode criar, editar e excluir categorias e sub categorias.

4. O lojista inicialmente pode enviar uma imagem por loja e três imagens por produto. Podendo escalar esses recursos contratando um plano para sua necessidade.

5. O administrador pode alterar o controle de acesso, cargo e plano de usuários que não sejam do mesmo cargo.

6. Entrega de e-mail para o usuário informando uma mudança de estado em dados críticos.

7. Solicitação de mudança de senha e mudança de e-mail enviam uma mensagem de confirmação para o e-mail cadastrado contendo um link de uso único para efetuar a alteração.

8. Listar produtos por categoria
O administrador e o lojista tem acesso ao painel
O lojista pode criar, editar e excluir lojas e administrar os produtos de cada loja.


