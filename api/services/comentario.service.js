const { comentario, produto } = require('../models/index');
const cria = async (req) => {
  let body = {...req.body}

  if([1,-1].includes(body['tipo'])){


    var doc = await produto.findOneAndUpdate({_id:body['produto']},{
      useFindAndModify: false
    })

    if(doc.usuarioVotante.includes(req.userId)){
      return {status:false,mensagem:"Você já votou nesse produto."}
    }
    await comentario.create(body);

    doc.usuarioVotante.push(req.userId)
    let curtida = doc.curtida || 0
    let calc = curtida + parseInt(body['tipo'])
    doc.curtida = calc

    await doc.save()
    return {status:true,mensagem:"Obrigado pelo feedback. <3"}
  }else{
    return {status:false,mensagem:"Você precisa selecionar um tipo de voto."}
  }

}
const get = async (req) => {
  const { id } = req.params
  if(id){
    return await comentario.findOne({_id:id})
  }
  return await comentario.find({})
}
const edit = async (req) => {
  let body = {...req.body}
  const { id } = req.params
  return comentario.updateOne({_id:id}, body);
}
const editRelevance = async (req) => {
  let update = {...req.body}
  const { id } = req.params
  console.log('edit relevance', new Date())
  var doc = await comentario.findOneAndUpdate({_id:id},{
    useFindAndModify: false
  })

  if(doc.usuarioVotante.includes(req.userId)){
    return {status:false,mensagem:"Você já votou nesse comentário."}
  }
  doc.usuarioVotante.push(req.userId)
  let calc = doc.relevante + parseInt(update['relevante'])
  // if(calc >= 0){
  doc.relevante = calc
  // }
  await doc.save()
  return {status:true,mensagem:"Obrigado pelo feedback. <3"}
}

const lista = async (_id) => {
  if(_id){
    return comentario.findOne({_id})
  }
  return comentario.find({})
}
const deleteComentario = async (req) => {
  const { id } = req.params
  try{
    var comentarioDoc = await comentario.findOneAndDelete({_id:id},{
      useFindAndModify: false
    })
    var produtoDoc = await produto.findOneAndUpdate({_id:comentarioDoc.produto},{
      useFindAndModify: false
    })
    let userIndex = produtoDoc.usuarioVotante.findIndex((item) => item.equals(comentarioDoc.usuario))
    produtoDoc.usuarioVotante.splice(userIndex,1)
    let produtoCurtida =  produtoDoc.curtida
    let calc = produtoCurtida + (comentarioDoc.tipo * -1)
    produtoDoc.curtida = calc
    await produtoDoc.save()
    return {status:true,mensagem:"Obrigado pelo feedback. <3"}
  }catch(Exception){
    //  console.log(Exception)
    return {status:false,mensagem:"Ocorreu um erro ao remover seu comentário."}
  }

}
const listaByProduct = async (req) => {
  const { id } = req.params
  let { page = 1, limit = 5, relevante = 0, date = 0} = req.query
  let usuario = req.userId || ''
  console.log(req.query)
  relevante = parseInt(relevante)
  date = parseInt(date)
  let json_finder = { produto:id}
  let usuarioLastComment = {}
  let sortArray = [['createdAt', date],['relevante', relevante]]
  if([1,-1].includes(relevante) == false){
    let index =  sortArray.findIndex((item)=> item[0] === 'relevante')
    sortArray.splice(index,1)
  }
  if([1,-1].includes(date) == false){
    let index =  sortArray.findIndex((item)=> item[0] === 'createdAt')
    sortArray.splice(index,1)
  }
  let usuarioExists
  if(usuario.length > 0){
    usuarioExists = await comentario.findOne({usuario}) ? true : false
    usuarioLastComment = await comentario.findOne({usuario}).populate('usuario',['nome','imagem'])
    json_finder = {produto:id,usuario:{$ne:usuario}}
  }
  let comentarios = await comentario.find(json_finder).populate('usuario',['nome','imagem']).sort(sortArray).limit(limit * 1).skip((page - 1)* limit)
  if(usuario.length > 0 && usuarioExists){
    comentarios.unshift(usuarioLastComment)
  }
  let calcPages = await comentario.countDocuments(json_finder) % limit
  let totalPage = 0
  if(calcPages != 0 ){
    totalPage++
  }
  totalPage += Math.trunc(await comentario.countDocuments(json_finder) / limit)//ignore

  return { currentPage: parseInt(page),totalPage,total:comentarios.length,comentarios }
}
module.exports = {
  cria,
  editRelevance,
  listaByProduct,
  lista,
  deleteComentario,
  get,
  edit
}
