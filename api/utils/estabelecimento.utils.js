const { estabelecimento, produto } = require('../models/index');
const {tagsGenerator} = require('../utils/search-engine')

const updateTags  = async (model) =>{
    const tags =  await generateAllTags({id:model.estabelecimento})
console.log('creating tags..',tags)
    const doc = await estabelecimento.findOneAndUpdate({ _id:model.estabelecimento },{
        useFindAndModify: false
       })

   tags.map((item)=>{
       if(!doc.tags.includes(item)){
           doc.tags.push(item)
       }
   })

      return await doc.save()
}
const removeTags = async (model) =>{

   const servicoModel = await produto.findOne({_id:model.id});
    const tags = tagsGenerator(servicoModel)
    const doc = await estabelecimento.findOneAndUpdate({ _id:servicoModel.estabelecimento },{
        useFindAndModify: false
       })

     doc.tags = doc.tags.filter(item => !tags.includes(item))
   return await doc.save()

}
const getEstabelecimentoCount = async (req) => {
    return await estabelecimento.countDocuments({ usuario:req.userId })
}
const generateAllTags = async (model) =>{
    const servicoModel = await produto.find({estabelecimento:model.id});
    const estabelecimentoModel = await estabelecimento.findOne({_id:model.id});
    var tagsServico = []
    if(servicoModel){
        servicoModel.map((item)=>{
            const {planos,servicos,nome} = item
            tagsServico = tagsServico.concat(tagsGenerator({planos,servicos,nome}))
        })
        }
        var tagsNome = []
        const {nome} = estabelecimentoModel
    tagsNome = tagsGenerator({nome})
    console.log('modelgenerate',tagsServico.concat(tagsNome))

    return tagsServico.concat(tagsNome)
}

module.exports = {
    updateTags,
    removeTags,
    getEstabelecimentoCount,
    generateAllTags
}