// const mogoose = require('mongoose');
const cargo = require('./cargo')
const { Schema } = require("mongoose");


const usuarioSchema = {

  email: {
    type: String,
    required: true,
  },
  senha: {
    type: String,
    required: true,
  },
  nome: {
    type: String,
    required: true,
  },
  cpf: {
    type: String,
    required: true,
  },
  plano: {
    type: String,
    required: false,
  },
  extraPermission: {
    type: Array,
    required: false,
  },
  status: {
    type: Number,
    default: 0,
    required: false,
  },
  date: {
    type: Date,
    required: false,
  },
  imagem: {
    type : Object,
    required:false
  },

  plano: {type: Schema.Types.ObjectId, ref:'plano'},

  cargo: {type: Schema.Types.ObjectId, ref:'cargo'}
}


module.exports = usuarioSchema;



