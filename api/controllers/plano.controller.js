const planoService = require('../services/plano.service');


const createPlano= async (req,res,next) => {
  planoService.cria(req.body)
  console.log('req.body',req.body)
  return res.status(200).send({
    mensagem: "Plano registrado com sucesso."
  });
}
const getPlano = async (req, res, next) => {
    let response = await planoService.get(req)
    return res.status(200).json(response);

  }

const delPlano = (req, res, next) => {
  planoService.del(req.body)
  return res.status(200).send({
    mensagem: "Plano deletado com sucesso."
  });

}
const editPlano = async (req,res,next) => {
  planoService.edit(req.body)

  return res.status(200).send({
    mensagem: "Plano alterado com sucesso."
  });
}

module.exports = {
  createPlano,
  delPlano,
  getPlano,
  editPlano
}