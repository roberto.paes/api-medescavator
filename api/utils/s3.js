const S3 = require('aws-sdk/clients/s3')
const fs = require('fs')
const path = require('path');

const bucketName = process.env.AWS_BUCKETNAME
const region = process.env.AWS_DEFAULT_REGION
const accessKeyId = process.env.AWS_ACESS_KEY_ID
const secretAccessKey = process.env.AWS_SECRET_ACESS_KEY
const tmpDirectory = path.join(path.dirname(require.main.filename),'tmp')

const s3 = new S3({
    region,
    accessKeyId,
    secretAccessKey
})
const MAX_SIZE_TWO_MEGABYTES = 2 * 1024 * 1024;

function uploadFileResize(file,destino,callback){
    let fileFolder = path.join(tmpDirectory,file)
    fs.readFile(fileFolder, (err, data) => {
        if (err) throw err;
        const params = {
            Bucket:`${bucketName}/${destino}`,
            Body: data,
            limits: {
                fileSize: MAX_SIZE_TWO_MEGABYTES
            },
            ACL: "public-read",
            Key: file, // file will be saved as testBucket/contacts.csv
        };
        s3.upload(params).promise()
        if(callback){
            return callback()
        }
    });
}
function uploadFile(file,destino,callback){
    const fileStream = fs.createReadStream(file.path)
    const uploadParams = {
        Bucket:`${bucketName}/${destino}`,
        Body:fileStream,
        limits: {
            fileSize: MAX_SIZE_TWO_MEGABYTES
        },
        ACL: "public-read",
        Key:file.filename //+ '.'+ file.mimetype.split('/')[1]
    }
    s3.upload(uploadParams).promise()
    if(callback){
        return callback()
    }
}
function updateFile(old_image,imageIncludes,destino,callback){
    old_image.map((img)=>{
        if(imageIncludes == null){
            const deleteParams = {
                Bucket:`${bucketName}/${destino}`,
                Key:img.name
            }
            s3.deleteObject(deleteParams, function(err, data) {
                if (err) console.log(err, err.stack)
                else     console.log(data)
            }).promise()
        }else{
            if(!imageIncludes.includes(img.name)){
                const deleteParams = {
                    Bucket:`${bucketName}/${destino}`,
                    Key:img.name
                }
                s3.deleteObject(deleteParams, function(err, data) {
                    if (err) console.log(err, err.stack)
                    else     console.log(data)
                }).promise()
            }
        }
    })
    if(callback){
        return callback()
    }
}
function deleteFile(file,destino,callback){
    const deleteParams = {
        Bucket:`${bucketName}/${destino}`,
        Key:file
    }
    s3.deleteObject(deleteParams, function(err, data) {
        if (err) console.log(err, err.stack);
        else     console.log(data);
    })
    if(callback){
        return callback()
    }
}

module.exports = {
    uploadFile,
    updateFile,
    uploadFileResize,
    deleteFile
}

