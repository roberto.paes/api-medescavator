

const planoSchema = {

  nome: {
    type: String,
    required: true,
  },
  preco: {
    type: Number,
    required: true,
  },
  ciclo: {
    type: String,
    default: 'mes',
    required: true,
  },
  permissions: {
    type: Array,
    required: false,
  },
  extras: {
    type: Array,
    required: false,
  },
  status: {
    type: Boolean,
    default: true,
    required: false,
  },
  descricao: {
    type: String,
    required: false,
  }
}


module.exports = planoSchema;



