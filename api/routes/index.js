const { Router } = require('express');
const { name, version } = require('../../package.json');


const routesV1Usuarios = require('./v1/usuario');
const routesV1Cargo = require('./v1/cargo');
const routesV1Estabelecimento = require('./v1/estabelecimento');
const routesV1Produto = require('./v1/produto');
const routesV1Categoria = require('./v1/categoria');
const routesV1Plano = require('./v1/plano');
const routesV1Comentario = require('./v1/comentario');

const routestTestPermission = require('./teste/permissionTest');


module.exports = (app) => {

  app.get('/', (req, res, next) => {
    res.send({ name, version });
  });

  const routesV1 = Router();
  // const routesTeste = Router();

  routesV1Usuarios(routesV1);
  routesV1Cargo(routesV1)
  routesV1Estabelecimento(routesV1)
  routesV1Produto(routesV1)
  routesV1Categoria(routesV1)
  routesV1Plano(routesV1)
  routesV1Comentario(routesV1)
  //routesV1(routesV1)
  app.use('/v1', routesV1);



  // routestTestPermission(routesTeste)
  // app.use('/teste', routesTeste);



}