const Categoria = require('../services/categoria.service');
const {tagsUpdateMiddleware} = require('../utils/middlewares/produto/tags.middleware')


const createCategoria= async (req,res,next) => {
  await Categoria.cria(req.body)
  console.log('req.body',req.body)
  return res.status(200).send({
    mensagem: "Categoria registrado com sucesso."
  });
}

const delCategoria = async (req, res, next) => {
  await Categoria.del(req.params.id)
  await tagsUpdateMiddleware('categoria',req)

  return res.status(200).send({
    mensagem: "Categoria deletado com sucesso."
  });

}
const advancedList = async (req,res,next) => {
  const lista = await Categoria.listaAvancada(req.params.params,req.params.value)

  return res.status(200).send(lista)
}
const lista = async (req,res,next) => {
  const lista = await Categoria.lista(req.params.id)

  return res.status(200).send(lista)
}
const listaGlobal = async (req,res,next) => {
  const lista = await Categoria.listaGlobals()

  return res.status(200).send(lista)
}
const editCategoria = async (req,res,next) => {
;

  await Categoria.edit(req.params.id,req.body)
  await tagsUpdateMiddleware('categoria',req)

  return res.status(200).send({
    mensagem: "Categoria alterado com sucesso."
  });
}

module.exports = {
  createCategoria,
  listaGlobal,
  delCategoria,
  advancedList,
  lista,
  editCategoria
}