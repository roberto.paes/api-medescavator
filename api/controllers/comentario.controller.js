const comentarioService = require('../services/comentario.service');


const createComentario= async (req,res,next) => {
  let result = await comentarioService.cria(req)
  if(result.status){
    return res.status(200).send({
      mensagem:result.mensagem
    });
  }else{
    return res.status(404).send({
      mensagem:result.mensagem
    });
  }
}
const getComentario = async (req, res, next) => {
  let response = await comentarioService.get(req)
  return res.status(200).json(response);

}


const relevanteComentario = async (req,res,next) => {
  let result = await comentarioService.editRelevance(req)
  if(result.status){
    return res.status(200).send({
      mensagem:result.mensagem
    });
  }else{
    return res.status(404).send({
      mensagem:result.mensagem
    });
  }

}
const editComentario = async (req,res,next) => {
  comentarioService.edit(req.body)

  return res.status(200).send({
    mensagem: "Comentario alterado com sucesso."
  });
}
const getComentarioByProduto = async (req, res, next) => {


    let comentario = await comentarioService.listaByProduct(req)
    return res.status(200).json(comentario);

}
const lista = async (req, res, next) => {

  try {
    const estabelecimentos = await estabelecimentoService.lista(req.params.id,req)
    return res.status(200).send(estabelecimentos);
  } catch (err) {

    return res.status(201).send(err);

  }
}
const deleteComentario = async (req, res, next) => {


  const result = await comentarioService.deleteComentario(req)
  if(result.status){
    return res.status(200).send({
      mensagem:result.mensagem
    });
  }else{
    return res.status(404).send({
      mensagem:result.mensagem
    });
  }

}
module.exports = {
  createComentario,
  deleteComentario,
  relevanteComentario,
  getComentarioByProduto,

  lista,
  getComentario,
  editComentario
}