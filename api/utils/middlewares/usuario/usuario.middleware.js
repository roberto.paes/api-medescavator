const { requirePermission } = require('../permission.middleware')

async function usuarioMiddleware (req,res, next) {

    let rank = 0
    if(requirePermission(['RANK_1'], req)){
        rank = 1
    }
    else if(requirePermission(['RANK_2'], req)){
        rank = 2
    }
    else if(requirePermission(['RANK_3'], req)){
        rank = 3
    }
    else if(requirePermission(['RANK_4'], req)){
        rank = 4
    }
    else if(requirePermission(['RANK_5'], req)){
        rank = 5
    }
    req.rank = rank
    req.numberImage = 1

    next()


}

module.exports = {
    usuarioMiddleware
}