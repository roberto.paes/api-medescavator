const { categoria,estabelecimento, produto } = require('../models/index');
const {updateTags,removeTags,generateAllTags} = require('../utils/estabelecimento.utils')
const cria = async (body) => {
  if(Array.isArray(body['categorias'])){
    body['global'] = true
  }else{
    body['global'] = false
  }
  await categoria.create({...body});
}
const edit = async (_id,body) => {
  const updateBody = {...body}

  var doc = await categoria.findOneAndUpdate({_id}, updateBody,{
    useFindAndModify: false
  })
  if(Array.isArray(body['categorias'])){
    doc['global'] = true
  }else{
    doc['global'] = false
    doc['categorias'] = undefined
  }
  //await tagsUpdateMiddleware(_id,'produto','categoria','categorias',['nome','modelo','fabricante'])
  await doc.save()
}
const del = async (id) => {
  let produtos = await produto.find({categorias:id})
  await Promise.all(produtos.map(async(item)=>{
    let categorias = item.categorias
    var index = categorias.findIndex((item) => item == id)
    categorias.splice(index,1)
    await produto.updateOne({_id:item._id}, {categorias:categorias});
  }))
  await categoria.deleteOne({_id:id})


}

const lista = async (_id) => {
  if(_id){
    return categoria.findOne({_id})
  }
  return categoria.find({})
}
const listaAvancada = async (params,value) => {
  if(params){
    return categoria.find({[params]:value})
  }
  return categoria.find({})
}
const listaGlobals = async () => {

  return categoria.aggregate(
    [
      { $match : { global : true } },
      {$lookup: {
        from: 'categoriaCollection',
        localField: 'categorias',
        foreignField: '_id',
        as: 'categorias'
      }
    },
    {
      $match: {
        "categorias.status": true
      }
    },
  ]
  );

  //return categoria.find({global:true})
}
module.exports = {
  cria,
  lista,
  listaGlobals,
  del,
  listaAvancada,
  edit
}
