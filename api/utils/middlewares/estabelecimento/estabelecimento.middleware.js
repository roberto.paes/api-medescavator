const { getEstabelecimentoCount } = require('../../estabelecimento.utils')
const { requirePermission } = require('../permission.middleware')

async function estabelecimentoMiddleware (req,res, next) {

    console.log(req.method)
    let lojaMax = 1
    switch(req.method){
        case 'POST':
        if(requirePermission(['EST_3'], req)){
            lojaMax = 3
        }else if(requirePermission(['EST_5'], req)){
            lojaMax = 5

        }else if(requirePermission(['EST_7'], req)){
            lojaMax = 7
        }
        else if(requirePermission(['EST_9'], req)){
            lojaMax = 9
        }
        else if(requirePermission(['EST_11'], req)){
            lojaMax = 11
        }
        let lojaCount  = await getEstabelecimentoCount(req)
        if(lojaCount >= lojaMax){
            return res.status(404).json({error:"Número máximo de lojas atingido."})
        }
        break
    }
    let numberImage = 1
    if(requirePermission(['EST_IMG_2'], req)){
        numberImage = 2
    }
    req.numberImage = numberImage

    let rank = 0
    if(requirePermission(['RANK_1'], req)){
        rank = 1
    }
    else if(requirePermission(['RANK_2'], req)){
        rank = 2
    }
    else if(requirePermission(['RANK_3'], req)){
        rank = 3
    }
    else if(requirePermission(['RANK_4'], req)){
        rank = 4
    }
    else if(requirePermission(['RANK_5'], req)){
        rank = 5
    }
    req.rank = rank

    next()


}

module.exports = {
    estabelecimentoMiddleware
}