
const comentarioController = require('../../controllers/comentario.controller');
const validaDTO = require('../../utils/middlewares/validate-dto.middleware');
const { requirePermissionsMiddleware, verifyJWT, verifyJWTOptional} = require('../../utils/middlewares/permission.middleware')

const joi = require('joi');

module.exports = (router) => {

    router.route('/comentario')
    .post(verifyJWT,requirePermissionsMiddleware(["CREATE_COMMENT"]),
    comentarioController.createComentario
    )
    .get(
        comentarioController.lista
        )
        router.route('/comentario/:id')
        .get(
            comentarioController.lista
            )
            router.route('/comentario/produto/:id')
            .get(verifyJWTOptional,
                comentarioController.getComentarioByProduto
                )
                    router.route('/comentario/:id')
                    .delete(
                        comentarioController.deleteComentario
                        )
                        router.route('/comentario/:id')
                        .put(verifyJWT,requirePermissionsMiddleware(["EDIT_COMMENT"]),
                        comentarioController.editComentario
                        )
                        router.route('/comentario/vote/:id')
                        .put(verifyJWT,requirePermissionsMiddleware(["VOTE_COMMENT"]),
                        comentarioController.relevanteComentario
                        )

                    }