const { cargo } = require('../models/index');
const cria = async (body) => {
  return cargo.create(body);
}
const edit = async (body) => {
  return cargo.updateOne({_id:body.id}, body);
}
const del = async (body) => {
  return cargo.deleteOne({_id:body.id});
}
const getAll = async () => {
  return cargo.find({_id:{$ne:"222222222222222222222222"}});
}

module.exports = {
  cria,
  del,
  getAll,
  edit
}
