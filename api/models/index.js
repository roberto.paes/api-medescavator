const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const createSchema = (modelPai, model, options = {}) => {

  return new Schema({
    ...modelPai,
    ...model,
  }, {
    timestamps: true,
    collection: 'usuarioCollection',
    ...options,
  })

}


// ## USUARIO

const usuarioSchema = require('./usuario');
const usuario = mongoose.model('usuario', createSchema(undefined, usuarioSchema, {
  discriminatorKey: 'kind',
}));

const EstabelecimentoSchema = require('./estabelecimento');
const estabelecimento = mongoose.model('estabelecimento', createSchema(undefined, EstabelecimentoSchema, {
  collection: 'estabelecimentoCollection',
  versionKey: false,
  timestamps:false
}));

const cargoSchema = require('./cargo');
const cargo = mongoose.model('cargo', createSchema(undefined, cargoSchema, {
  collection: 'cargoCollection',
  timestamps:false
}));

const comentarioSchema = require('./comentario');
const comentario = mongoose.model('comentario', createSchema(undefined, comentarioSchema, {
  collection: 'comentarioCollection',
  timestamps:true
}));

// ## CATEGORIA



const categoriaSchema = require('./categoria');
const categoria = mongoose.model('categoria', createSchema(undefined, categoriaSchema, {
  collection: 'categoriaCollection',
  timestamps:false
}));

const planoSchema = require('./plano');
const plano = mongoose.model('plano', createSchema(undefined, planoSchema, {
  collection: 'planoCollection',
  timestamps:false
}));

const produtoSchema = require('./produto');
produtoNewSchema = createSchema(undefined, produtoSchema, {
  collection: 'produtoCollection',
  versionKey: false,
  timestamps:false,
  toJSON: { getters: true }
})
// produtoNewSchema.path('preco').get(function(num) {
//   //console.log('get worked',num)
//   return parseFloat((num / 100).toFixed(2));
// });
// produtoNewSchema.path('preco').set(function(num) {
//  // console.log('set worked',num)
//   return num * 100;
// });

const produto = mongoose.model('produto',produtoNewSchema);


module.exports = {

  cargo,
  categoria,
  plano,
  produto,
  comentario,
  usuario,
  estabelecimento,
}

