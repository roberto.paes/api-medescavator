const permissionTestController = require('../../controllers/teste/permission.test.controller')
const {requirePermissions,verifyJWT} = require('../../utils/middlewares/permission.middleware')

module.exports = (router) => {

    router.route('/permission')
    .get(verifyJWT,requirePermissions(["EDIT_USER",'TESTE']),
        permissionTestController.teste
        )

    }