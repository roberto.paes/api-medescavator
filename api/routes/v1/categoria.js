const validaDTO = require('../../utils/middlewares/validate-dto.middleware');
const categoriaController = require('../../controllers/categoria.controller');
const joi = require('joi');
const {requirePermissionsMiddleware,verifyJWT} = require('../../utils/middlewares/permission.middleware')
const {tagsGeneratorMiddleware,tagsUpdateMiddleware} = require('../../utils/middlewares/produto/tags.middleware')

module.exports = (router) => {

  router.route('/categoria/global')
  .get(
    categoriaController.listaGlobal
    )
    router.route('/categoria').get(
      categoriaController.lista
      )
      router.route('/categoria')
      .post(verifyJWT,requirePermissionsMiddleware(['CREATE_CATEGORY']),
      validaDTO('body', {
        nome: joi.string().required().messages({
          'any.required': `"nome" é um campo obrigatório`,
          'string.empty': `"nome" não deve ser vazio`,
        }),
        descricao: joi.string().required().messages({
          'any.required': `"descricao" é um campo obrigatório`,
          'string.empty': `"descricao" não deve ser vazio`,
        }),
        status: joi.boolean().required().messages({
          'any.required': `"status" é um campo obrigatório`,
          'booleam.empty': `"status" não deve ser vazio`,
        }),
        global: joi.boolean().messages({
          'any.required': `"status" é um campo obrigatório`,
          'booleam.empty': `"status" não deve ser vazio`,
        }),
        categorias: joi.array().allow(null).optional(),
        index: joi.number().messages({
          'any.required': `"status" é um campo obrigatório`,
          'booleam.empty': `"status" não deve ser vazio`,
        }),

      }),
      categoriaController.createCategoria
      )
      router.route('/categoria/:id')
      .get(
        categoriaController.lista
        )
        router.route('/categoria/:id').put(verifyJWT,requirePermissionsMiddleware(["EDIT_CATEGORY"]),
        categoriaController.editCategoria
        )
        router.route('/categoria/:id').delete(verifyJWT,requirePermissionsMiddleware(["DELETE_CATEGORY"]),
        categoriaController.delCategoria
        )
        router.route('/categoria/advanced/:params/:value').get(
          categoriaController.advancedList
          )
        }
