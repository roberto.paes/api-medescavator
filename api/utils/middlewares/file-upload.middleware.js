const path = require('path');
const fs = require('fs');
// const formidable = require('formidable');
const multer = require('multer')
const tmpDirectory = path.join(path.dirname(require.main.filename),'tmp')
const { uploadFileResize,uploadFile } = require('../s3')
const { requirePermission } = require('../middlewares/permission.middleware')
const sharp = require('sharp');
const uuid = require('uuid').v4
//const upload = multer({dest:tmpDirectory})

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, tmpDirectory)
  },
  filename: function (req, file, cb) {
    cb(null, uuid() + path.extname(file.originalname)) //Appending extension
  }
})
var upload = multer({ storage: storage });
const fileUpload = (destino,resize = true) => {

  return async (req, res, next) => {
    upload.array('imageUpload',req.numberImage)(req, res, async function (err) {
      var include = []

      if(req.body.imageInfo && Array.isArray(req.body.imageInfo)){

        req.body.imageInfo.map((item)=>{
          if(JSON.parse(item) instanceof Object){
            include.push(JSON.parse(item))
          }
        })
      }else{
        if(req.body.imageInfo && JSON.parse(req.body.imageInfo) instanceof Object){
          include.push(JSON.parse(req.body.imageInfo))
        }
      }
      let filesCount = include.length
      if (err || filesCount > req.numberImage){
        return res.status(404).json({error:"Número máximo de upload atingido."})
      }
      if(!req.files){
        return next()
      }
      if(resize){
        const arrayImages = [];
        let i;
        for (i = 0; i< req.files.length; i++) {
          const { filename : image } = req.files[i]
          let filename = `${uuid()}${path.extname(req.files[i].originalname)}`
          await sharp(req.files[i].path)
          .resize(500,500) // redimensionando para 500px
          .toFile(path.resolve(req.files[i].destination, filename))
          arrayImages.push({filename,originalname:req.files[i].originalname,mimetype:path.extname(req.files[i].originalname)}); // adicionando no vetor
          fs.unlinkSync(req.files[i].path);
        }
        arrayImages.map((file)=>{
          uploadFileResize(file.filename,destino,function() {
            if(fs.existsSync(path.join(tmpDirectory,file.filename))){
              fs.unlinkSync(path.join(tmpDirectory,file.filename))
            }
          })
        })
        req.body = {...req.body,imagens:arrayImages,includeImage:include}

      }else{
        console.log('filemiddleware',req.files)

        req.files.map((file)=>{
          uploadFile(file,destino,function() {
            if(fs.existsSync(path.join(tmpDirectory,file.filename))){
              fs.unlinkSync(path.join(tmpDirectory,file.filename))
            }
          })
        })
        req.body = {...req.body,imagens: req.files,includeImage:include}


      }


      // console.log('filemiddleware',req.body)

      next()
    })




  }
}








module.exports = fileUpload;
